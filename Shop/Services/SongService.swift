//
//  SongService.swift
//  Shop
//
//  Created by CALC Company on 14/03/2023.
//

import Foundation
struct MusicData : Codable{
    let tracks : [MusicTrack]
}
 struct MusicTrack : Codable{
    let data : [MusicTrackData]
     let total: Int
 }
struct MusicTrackData: Codable {
    let id: Int
    let title: String
    let title_short: String
    let title_version: String
    let link: String
    let duration: Int
    let rank: Int
    let explicit_lyrics: Bool
    let explicit_content_lyrics: Int
    let explicit_content_cover: Int
    let preview: String
    
}
class SongService {
    func fetchTrendingMusic(completion: @escaping ([MusicData]?, Error?) -> Void) {
           let urlString = "https://api.deezer.com/chart"
           guard let url = URL(string: urlString) else {
               completion(nil, NSError(domain: "Invalid URL", code: 0, userInfo: nil))
               return
           }
           
           URLSession.shared.dataTask(with: url) { data, response, error in
               if let error = error {
                   completion(nil, error)
                   return
               }
               
               guard let data = data else {
                   completion(nil, NSError(domain: "Invalid Data", code: 0, userInfo: nil))
                   return
               }
               
               do {
                   let decoder = JSONDecoder()
                   let tracks = try decoder.decode([MusicData].self, from: data)
                   print(tracks)
                   completion(tracks, nil)
               } catch let error {
                   completion(nil, error)
               }
           }.resume()
       }
}
