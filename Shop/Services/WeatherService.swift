import SwiftUI
import CoreLocation



class WeatherService {
    let apiKey = "b7facf4751579e38af8f618a713b62f4"

    func getWeather(forCoordinates coordinates: CLLocationCoordinate2D,  completion: @escaping (Result<WeatherData, Error>) -> Void) {
        guard let urlString =
                "https://api.openweathermap.org/data/2.5/weather?lat=\(coordinates.latitude)&lon=\(coordinates.longitude)&appid=\(apiKey)&units=metric"
              .addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else { return }
          guard let url = URL(string: urlString) else { return }
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            if let error = error {
                completion(.failure(error))
                return
            }

            guard let httpResponse = response as? HTTPURLResponse,
                  (200...299).contains(httpResponse.statusCode) else {
                completion(.failure(NSError(domain: "HTTPError", code: -1, userInfo: nil)))
                return
            }

            guard let data = data else {
                completion(.failure(NSError(domain: "NoDataError", code: -1, userInfo: nil)))
                return
            }

            do {
                let weatherResponse = try JSONDecoder().decode(WeatherData.self, from: data)
                completion(.success(weatherResponse))
            } catch let error {
                completion(.failure(error))
            }
        }.resume()
    }
}
