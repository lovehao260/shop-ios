//
//  Webservice.swift
//  Shop
//
//  Created by CALC Company on 27/02/2023.
//


import SwiftUI
import Combine

enum AuthenticationError: Error {
    case invalidCredentials
    case custom(errorMessage: String)
}

enum NetworkError: Error {
    case invalidURL
    case noData
    case decodingError
}

struct LoginRequestBody: Codable {
    let login: String
    let password: String
    
}

struct LoginResponse: Codable {
    let token: String?
    let token_type: String?
    let message: String?
    let success: Bool?
}

struct AccountViewModel: Codable{
    
    let data: [Account]
    let code: Int
    let error: Bool
    let message : String
    
}
struct RegisterResponse: Codable {
    let id: Int?
    let username: String?
    let name: String?
    let email: String?
    
}
struct RegisterResponseError: Codable {
    let id: Int?
    let username: String?
    let name: String?
    let email: String?
    
}


struct ForgotData: Codable {
    let error: Bool
    let data: OtpData
    let message : String
}

struct ApiRespone: Codable {
    let error: Bool
    let data: [String]
    let message : String
}

struct Logout: Codable {
    let message : String
}


class Webservice {
    
    func getAllAccounts(token: String, completion: @escaping (Result<[Account], NetworkError>) -> Void) {
        
        guard let url = URL(string: "\(Constants.serverKey)users") else {
            completion(.failure(.invalidURL))
            return
        }
        
        var request = URLRequest(url: url)
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            
            guard let data = data, error == nil else {
                completion(.failure(.noData))
                return
            }
            
            let users = try? JSONDecoder().decode(AccountViewModel.self, from: data)
            
            if let users = users {
                DispatchQueue.main.async {
                    completion(.success(users.data))
                }
                
            } else {
                completion(.failure(.noData))
            }
            
        }.resume()
        
    }
    func deleteUser (token:String, id: Int, completion: @escaping (Result<String, NetworkError>) -> Void){
        
        let prefixUrl = "\(Constants.serverKey)users/destroy/\(id)"
        
        guard let url = URL(string: "\(prefixUrl)") else {
            completion(.failure(.invalidURL))
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "DELETE"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        URLSession.shared.dataTask(with: request) { data, res, error in
            if error != nil {
                print("error", error?.localizedDescription ?? "")
                return
            }
            
            guard let data = data, error == nil else {
                completion(.failure(.noData))
                return
            }
            
            let users = try? JSONDecoder().decode(AccountViewModel.self, from: data)
            
            if let users = users {
                DispatchQueue.main.async {
                    completion(.success(users.message))
                }
                
            } else {
                completion(.failure(.noData))
            }
            
        }.resume()
    }
    
    func updateUser(token: String, id:Int ,formData: FormData, image: UIImage? ,completion: @escaping (Result<String, NetworkError>) -> Void){
        
        let prefixUrl = "\(Constants.serverKey)users/update/\(id)"
        let formFields = [
            "name": formData.name,
            "username": formData.username,
            "email": formData.email
        ]
        guard let url = URL(string: "\(prefixUrl)") else {
            completion(.failure(.invalidURL))
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        
        
        // Add form data to the request body
        let boundary = "Boundary-\(UUID().uuidString)"
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        let imageData = image?.jpegData(compressionQuality: 0.5)
        
        let body = NSMutableData()
        
        // Add form data to the request body
        for (fieldName, fieldValue) in formFields {
            body.append("--\(boundary)\r\n".data(using: .utf8)!)
            body.append("Content-Disposition: form-data; name=\"\(fieldName)\"\r\n\r\n".data(using: .utf8)!)
            body.append("\(fieldValue)\r\n".data(using: .utf8)!)
        }
        
        
        // Add image data to the request body
        if let imageData = imageData {
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition: form-data; name=\"avatar\"; filename=\"avatar.jpeg\"\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Type: image/jpeg\r\n\r\n".data(using: String.Encoding.utf8)!)
            body.append(imageData)
            body.append("\r\n".data(using: String.Encoding.utf8)!)
        }
        
        body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)
        
        request.httpBody = body as Data
        
        URLSession.shared.dataTask(with: request) { data, res, error in
            if error != nil {
                print("error", error?.localizedDescription ?? "")
                return
            }
            
            guard let response = res as? HTTPURLResponse,
                  (200...299).contains(response.statusCode) else {
                print ("server error")
                return
            }
            
            
        }.resume()
    }
    
    func login(username: String, password: String, completion: @escaping (Result<String, AuthenticationError>) -> Void) {
        
        guard let url = URL(string: "\(Constants.serverKey)login") else {
            completion(.failure(.custom(errorMessage: "URL is not correct")))
            return
        }
        
        let body = LoginRequestBody(login: username, password: password)
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = try? JSONEncoder().encode(body)
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard let data = data, error == nil else {
                completion(.failure(.custom(errorMessage: "No data")))
                return
            }
            
            
            guard let loginResponse = try? JSONDecoder().decode(LoginResponse.self, from: data) else {
                completion(.failure(.invalidCredentials))
                return
            }
            
            guard let token = loginResponse.token else {
                completion(.failure(.invalidCredentials))
                return
            }
            
            completion(.success(token))
            
            
        }.resume()
        
    }
    
    func register( formData: DataRegister,completion: @escaping (Result<String, AuthenticationError>) -> Void) {
        
        
        let parameters = [
            "name": formData.name,
            "username": formData.username,
            "email": formData.email,
            "password": formData.password,
            "password_confirmation": formData.confirmPassword
        ]
        let prefixUrl = "\(Constants.serverKey)register"
        let data = try! JSONSerialization.data(withJSONObject: parameters)
        let url = URL(string: "\(prefixUrl)")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = data
        URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            guard let data = data, error == nil else {
                completion(.failure(.custom(errorMessage: "No data")))
                return
            }
            
            do {
                try JSONDecoder().decode(RegisterResponse.self, from: data)
                
            } catch  {
                completion(.failure(.invalidCredentials))
                return
            }
            completion(.success("Success"))
        }.resume()
        
    }
    // completion: @escaping (Result<WeatherData, NetworkError>) -> Void) {
    func forgotPasswordEmail(emailForgot:Forgot, completion: @escaping (Result<ForgotData, NetworkError>) -> Void) {
        let parameters = [
            "email": emailForgot.email,
        ]
        
        let prefixUrl = "\(Constants.serverKey)password/email"
        let data = try! JSONSerialization.data(withJSONObject: parameters)
        let url = URL(string: "\(prefixUrl)")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = data
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {
                completion(.failure(.noData))
                return
            }
            
            do {
                let responseData = try JSONDecoder().decode(ForgotData.self, from: data)
                completion(.success(responseData))
            } catch  {
                completion(.failure(.noData))
            }
            
        }.resume()
    }
    
    func changePassword(formFields: PasswordChang, completion: @escaping (Result<ApiRespone, NetworkError>) -> Void) {
        
        let parameters = [
            "id": formFields.id,
            "otp": formFields.otp,
            "password": formFields.password,
            "password_confirmation": formFields.password_confirmation
        ]
        
        let prefixUrl = "\(Constants.serverKey)password/reset-password"

        let data = try! JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        let url = URL(string: "\(prefixUrl)")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = data
        URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {
                completion(.failure(.noData))
                return
            }
            
            do {
                let responseData = try JSONDecoder().decode(ApiRespone.self, from: data)
                completion(.success(responseData))
            } catch  {
                completion(.failure(.noData))
            }
            
        }.resume()
        
        
    }
    
    func getUserInfo(token: String, completion: @escaping (Result<Account, Error>) -> Void) {
        // Make an API request to get the user information using the token
  
        let prefixUrl = "\(Constants.serverKey)me"
        let url = URL(string: "\(prefixUrl)")!
               var request = URLRequest(url: url)
               request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
               
               URLSession.shared.dataTask(with: request) { data, response, error in
                   if let error = error {
                       completion(.failure(error))
                       return
                   }
                   
                   if let data = data {
                       do {
                           let user = try JSONDecoder().decode(AccountMe.self, from: data)
                           completion(.success(user.data))
                       } catch {
                           completion(.failure(error))
                       }
                   }
               }.resume()
    }
    
    func signout(token: String, completion: @escaping (Result<String, Error>) -> Void){
        let prefixUrl = "\(Constants.serverKey)me"
        let url = URL(string: "\(prefixUrl)")!
               var request = URLRequest(url: url)
               request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
               
               URLSession.shared.dataTask(with: request) { data, response, error in
                   if let error = error {
                       completion(.failure(error))
                       return
                   }
                   
                   if let data = data {
                       do {
                           let logout = try JSONDecoder().decode(Logout.self, from: data)
                           completion(.success(logout.message))
                       } catch {
                           completion(.failure(error))
                       }
                   }
               }.resume()
    }
}

struct APIError: LocalizedError {
    let message: String
    
    var errorDescription: String? {
        return message
    }
}

