//
//  OpenAiService.swift
//  Shop
//
//  Created by CALC Company on 30/05/2023.
//

import Foundation
import Alamofire
import Combine
class OpenAiService {
    let baseUrl = "https://api.openai.com/v1/"
    
    func sendMessage(message: String) -> AnyPublisher<OpenAICompletionsResponse, Error> {
        print(message)
        let body = OpenAICompletionsBody(
            model: "text-davinci-003",
            prompt: message,
            temperature: 0.7,
            max_tokens: 1000
        )
        
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(Constants.openAIAPIKey)"
        ]
        
        return Future { [weak self] promise in
              guard let self = self else { return }

              AF.request(
                self.baseUrl + "completions",
                method: .post,
                parameters: body,
                encoder: .json,
                headers: headers
              ).responseDecodable(of: OpenAICompletionsResponse.self) { response in
                  switch response.result {
                      case .success(let result):
                          promise(.success(result))
                      case .failure(let error):
                          promise(.failure(error))
                      }
                  }
              }
              .eraseToAnyPublisher()
    }
}

struct OpenAICompletionsBody: Encodable {
    let model: String // The language model
    let prompt: String // The message we want to send
    let temperature: Float?
    let max_tokens: Int?
}

struct OpenAICompletionsResponse: Decodable {
    let id: String
    let choices: [OpenAICompetionsChoice]
}

struct OpenAICompetionsChoice: Decodable {
    let text: String
}
