//
//  NewCartUIview.swift
//  Shop
//
//  Created by CALC Company on 21/02/2023.
//

import SwiftUI

struct NewUserUIview: View {
    @Binding var isPresented:Bool
    @StateObject  var registerVM = LoginViewModel()
    
    @StateObject  var accountListVM = AccountListViewModel()
    @State var formData = DataRegister()
    
    @State private var wrongName = 0;
    @State private var wrongEmail = 0;
    @State private var wrongUsername = 0;
    @State private var wrongPassword = 0;
    @State private var wrongPasswordConfirm = 0;
    
    var body: some View {
        NavigationView{
            ZStack{
                Color.gray.opacity(0.1).edgesIgnoringSafeArea(.all)
                VStack(alignment: .leading){
             
                    TextField("Name", text: $formData.name)
                        .padding()
                        .background(Color.white)
                        .cornerRadius(6)
                        .padding(.bottom)
                    TextField("User Name", text: $formData.username)
                        .padding()
                        .autocapitalization(.none)
                        .autocapitalization(.none)
                        .background(Color.white)
                        .cornerRadius(6)
                        .padding(.bottom)
                    TextField("Email", text: $formData.email)
                        .padding()
                        .autocapitalization(.none)
                        .background(Color.white)
                        .cornerRadius(6)
                        .padding(.bottom)
                    SecureField("password",text: $formData.password)
                        .autocapitalization(.none)
                        .padding()
                        .frame(width: 350, height:50)
                        .background(Color.white)
                        .cornerRadius(10)
                        .border(.red, width: CGFloat(wrongPassword))
                    
                    SecureField("passwordConfirm",text: $formData.confirmPassword)
                        .autocapitalization(.none)
                        .padding()
                        .frame(width: 350, height:50)
                        .background(Color.white)
                        .cornerRadius(10)
                        .border(.red, width: CGFloat(wrongPassword))
                    Spacer()
                }.padding()
          
            }
            .navigationBarTitle("New User", displayMode: .inline)
            .navigationBarItems(leading: leading,trailing: trading)
        }
    }
    
    var leading:some View{
        Button(action: {
            isPresented.toggle()
        }, label:{
            Text("Cancel")
        })
    }
    var trading:some View{
        Button(action: {
            validateCreateUser()
        }, label:{
            Text("Create")
        })
    }
    func validateCreateUser(){
        if isValidEmail(formData.email){
            wrongEmail = 0;
        } else {
            wrongEmail = 2;
        }
        
        if (isValidNull(formData.name)){
            wrongName = 0;
        } else {
            wrongName = 2;
        }
        
        if (isValidNull(formData.username)){
            wrongUsername = 0;
        }else{
            wrongUsername = 2;
        }
        if (isValidNull(formData.password)){
            wrongPassword = 0;
            
            if passwordsMatch(password: formData.password, confirmPassword: formData.confirmPassword){
                if (isValidInputLength(formData.password, min: 8, max: 20)){
                    wrongPassword = 0;
                    registerVM.registerHandler(formData: formData)
                    if !registerVM.showErrorAlert{
                        accountListVM.getAllAccounts()
                        isPresented.toggle()
                    }

                }else{
                    wrongPassword = 2;
                }
                
            }else{
                wrongPassword = 2;
            }
            
        }else{
            wrongPassword = 2;
        }
    }
   

}

//struct NewCartUIview_Previews: PreviewProvider {
//    static var previews: some View {
//        NewCartUIview()
//    }
//}
