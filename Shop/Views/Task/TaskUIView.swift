//
//  TaskUIView.swift
//  Shop
//
//  Created by CALC Company on 05/06/2023.
//

import SwiftUI
import LocalAuthentication

struct TaskUIView: View {
    @StateObject var taskModel: TaskViewModel = TaskViewModel()
    @Namespace var animation
    var body: some View {
        VStack{
            HeaderView()
            ScrollView(.vertical, showsIndicators: false){
                LazyVStack(spacing: 15, pinnedViews: [.sectionFooters]){
                    Section(header: HStack {
              
                    }) {
                        // MARK: Current week view
                        ScrollView(.horizontal, showsIndicators: false){
                            HStack(spacing:10){
                                ForEach(taskModel.currentWeek , id:\.self){ day in
                                    VStack(spacing: 10){
                                        Text(taskModel.extractDate(date: day, format: "dd"))
                                            .font(.system(size: 15))
                                            .fontWeight(.semibold)
                                        
                                        Text(taskModel.extractDate(date: day, format: "EEE"))
                                            .font(.system(size: 14))
                                        
                                        Circle().fill(.white)
                                            .frame(width: 8 , height: 8)
                                            .opacity(taskModel.isTodayOfWeek(date: day) ? 1 : 0)
                                    }
                                    .foregroundStyle(taskModel.isTodayOfWeek(date: day) ? .primary : .secondary)
                                    // MARK: Capsule shape
                                    .foregroundColor(taskModel.isTodayOfWeek(date: day) ? .white : .black)
                                    .frame(width: 45, height: 90)
                                    .background(
                                        ZStack{
                                            //MARK: Matched dometry effect
                                            if taskModel.isTodayOfWeek(date: day){
                                                Capsule().fill(.black)
                                                    .matchedGeometryEffect(id: "CURRENTDAY", in: animation)
                                            }
                                        }
                                   )
                                    .contentShape(Capsule())
                                    .onTapGesture {
                                        withAnimation{
                                            taskModel.currentDate = day
                                        }
                                    }
                                    
                                }
                            }
                        }
                     
                        TaskView()
                        .padding()
                    }
                }
          
            }
            .ignoresSafeArea(.container, edges: .top)
        }
    }
    
    //MARK: Header
    func TaskView()->some View{
        LazyVStack(spacing:18){
            if let tasks = taskModel.filteredTask{
                if tasks.isEmpty{
                    Text("Not found taks to day")
                        .font(.system(size: 16))
                        .fontWeight(.light)
                        .offset(y:100)
                }else{
                    ForEach(tasks){ task in
                        TaskCardView(task: task)
                    }
                }
            }else{
                Text("Not found taks to day")
                    .font(.system(size: 16))
                    .fontWeight(.light)
                    .offset(y:10)
                //MARK: Progress view
                ProgressView()
                    .offset(y: 10)
            }
        }
        .onChange(of: taskModel.currentDate){ newValue in
            taskModel.filterTodayTasks()
            
        }
    }
 
    
    //MARK: Card view task
    func TaskCardView(task: Task)->some View{
        HStack{
            VStack(spacing: 10) {
                Circle()
                    .fill(.black)
                    .frame(width: 15, height: 15)
                    .background(
                        Circle().stroke(.black, lineWidth: 1)
                            .padding(-3)
                    )
                Rectangle()
                    .fill(.black)
                    .frame(width: 3)
            }
            VStack{
                HStack(alignment: .top, spacing: 10) {
                    VStack(alignment: .leading, spacing: 12) {
                        Text(task.taskTitle)
                            .font(.title2.bold())
                        
                        Text(task.taskDescription)
                            .font(.callout)
                            .foregroundStyle(.secondary)
                        
                    }
                    .hLeading()
                    
                    Text(task.taskDate.formatted(date: .omitted, time: .shortened))
                }
            }
            .foregroundColor(.white)
            .padding()
            .hLeading()
            .background(Color(.black))
            .cornerRadius(25)
        }
        .hLeading()
      
    }
    
    //MARK: Header
    func HeaderView()->some View{
        HStack(spacing:10){
            VStack(alignment: .leading, spacing: 10){
                Text(Date().formatted(date: .abbreviated, time: .omitted))
                    .foregroundColor(.gray)
                Text("Today")
                    .font(.largeTitle)
            }
            .hLeading()
            
            Button{
                
            }label: {
                Image("google")
                    .resizable()
                    .aspectRatio( contentMode: .fill)
                    .frame(width: 45, height: 45)
                    .clipShape(Circle())
            }
        }
        .padding()
//        .padding(.top, getSafeArea().top)
        .background(Color.white)
        
    }
}

struct TaskUIView_Previews: PreviewProvider {
    static var previews: some View {
        TaskUIView()
    }
}


