//
//  MovieUIView.swift
//  Shop
//
//  Created by CALC Company on 17/03/2023.
//

import SwiftUI

struct MovieUIView: View {
    @StateObject var movieVM = MovieViewModel()
    @State var animate = false
    var body: some View {
        ZStack{
            Circle().fill(Color.green)
                .blur(radius: animate ? 30 : 100)
                          .frame(width: 200, height: 200)
                          .offset(x: animate ? -50: -130, y: animate ? -30 : -100)
            
            Circle().fill(Color.pink)
                .blur(radius: animate ? 30 : 100)
                          .frame(width: 300, height: 250)
                          .offset(x: animate ? 100: 130, y: animate ? 150 : 100 )
            
            VStack{
                Text("Move Ticket")
                    .font(.title3)
                    .foregroundColor(.white)
                    .fontWeight(.bold)
                CustomSearchBar()
            }
            .padding(.horizontal, 20)
            .frame(maxWidth: .infinity, maxHeight: .infinity , alignment: .top)
        }
        .background(
            LinearGradient(gradient: Gradient(colors: [Color("backgroundColor"),Color("backgroundColor1")]), startPoint: .top, endPoint: .bottom)
        )
    }
}





struct MovieUIView_Previews: PreviewProvider {
    static var previews: some View {
        MovieUIView()
    }
}

