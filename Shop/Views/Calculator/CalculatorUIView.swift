//
//  CalculatorUIView.swift
//  Shop
//
//  Created by CALC Company on 14/03/2023.
//

import SwiftUI

struct CalculatorUIView: View {
    @StateObject var calculateVM: CalculateViewModel = CalculateViewModel()
    @State private var isClicked = false
    var body: some View {
      
        ZStack{
            Color.black.edgesIgnoringSafeArea(.all)
            VStack{
                Spacer()
                HStack(alignment: .top){
                    Spacer()
                    Text(calculateVM.value)
                        .bold()
                        .font(.system(size: 72))
                        .foregroundColor(.white)
                }.padding()
                
                VStack{
                    ForEach(calculateVM.buttons, id:\.self) { row in
                        HStack(spacing: 12){
                            ForEach(row, id:\.self) { item in
                                Button(action: {
                                    calculateVM.didTap(button: item)
                                }, label: {
                                    Text(item.rawValue)
                                        .font(.system(size: 32))
                                        .frame(width:calculateVM.buttonWidth(item: item), height: calculateVM.buttonHeight(item: item))
                                        .background(item.buttonColor)
                                        .foregroundColor(.white)
                                        .cornerRadius(calculateVM.buttonWidth(item: item) / 2)
                                    
                                })
                            }
                        }.padding(.bottom,3)
                        
                    }
                }
            }
        }
        
    }
}

struct CalculatorUIView_Previews: PreviewProvider {
    static var previews: some View {
        CalculatorUIView()
    }
}
