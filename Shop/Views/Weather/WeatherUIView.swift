//
//  WeatherUIView.swift
//  Shop
//
//  Created by CALC Company on 06/03/2023.
//

import SwiftUI
import CoreLocation

struct WeatherUIView: View {
    @State private var isnight = false
    @StateObject var viewModel = WeatherViewModel()
    @State var isShowingMapModal = false

    @State private var selectedCoordinate: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: 0.0, longitude: 0.0)
    
    var body: some View {
        
        ZStack{
            
            BackgroundView(topColor: isnight ? .black : .blue, bottomColor: isnight ? .gray : Color("lightBlue"))
            
            VStack{
                CityTextName(cityName: viewModel.city )
                MainWeatherStatusView(imageName: viewModel.weatherIcon,temperature:viewModel.temperature)
                DetailWeatherView(weather:viewModel)
                Spacer()
                Button{
                    isnight.toggle()
                    self.isShowingMapModal.toggle()
                }label: {
                    WeatherButton(title: "Change Day time", textColor: .blue, backgroundColor: .white)
                }
                .foregroundColor(.white)
                .cornerRadius(8)
                .labelStyle(.titleAndIcon)
                .symbolVariant(.fill)
                .tint(.pink)
                .padding(.bottom, 50)
             
            }
            .sheet(isPresented: $isShowingMapModal, onDismiss: {
                viewModel.callWeather(coordinates:selectedCoordinate)
            }, content: {
                LocationUIView(isShowingMapModal:$isShowingMapModal, selectedCoordinate: $selectedCoordinate)
            })
            
        }
        
        .onAppear {
            viewModel.callWeather(coordinates:selectedCoordinate)
        }
    }
    
}


struct WeatherUIView_Previews: PreviewProvider {
    static var previews: some View {
        WeatherUIView()
    }
}


struct BackgroundView: View{
    var topColor: Color
    var bottomColor: Color
    var body: some View{
        
        LinearGradient(gradient: Gradient(colors: [topColor, bottomColor]),
                       startPoint: .topLeading, endPoint: .bottomTrailing)
        .edgesIgnoringSafeArea(.all) // full color background
    }
}

struct WeathereDayView: View{
    var dayOfWeek : String
    var imageName: String
    var temperature: Int
    
    var body: some View{
        VStack{
            Text(dayOfWeek)
                .font(.system(size: 16, weight: .medium, design: .default))
                .foregroundColor(.white)
            Image(systemName: imageName)
                .renderingMode(.original)
                .resizable()
                .aspectRatio(contentMode: .fit) // fit image
                .frame(width: 40, height: 40)
            Text("\(temperature)") //option + Shift + 8
                .font(.system(size: 25, weight: .medium))
                .foregroundColor(.white)
            
        }
    }
}

struct CityTextName: View{
    var cityName: String
    
    var body: some View{
        HStack{
            Text(cityName)
                .font(.system(size: 32,weight: .medium,design: .default))
                .foregroundColor(.white)
                .padding(.top,10)

            
        }
        
    }
}

struct MainWeatherStatusView: View{
    var imageName: String
    var temperature: String
    var body: some View{
        VStack{
            Image(systemName: imageName)
                .renderingMode(.original)
                .resizable()
                .aspectRatio(contentMode: .fit) // fit image
                .frame(width: 150, height: 150)
            Text("\(temperature)") //option + Shift + 8
                .font(.system(size: 58, weight: .medium))
                .foregroundColor(.white)
        }
        .padding(.bottom,10)
    }
}

struct DetailWeatherView: View{
    @StateObject var weather = WeatherViewModel()
    var body: some View{
        ZStack{
            LinearGradient(gradient: Gradient(colors: [.blue, .white]),
                           startPoint: .topLeading, endPoint: .bottomTrailing)
            .edgesIgnoringSafeArea(.all) // full color background
            
            VStack(alignment: .leading,spacing: 20){
                Text("Weather now").bold().padding(.bottom)
                HStack{
                    WeatherRow(logo: "thermometer.low", name: "Min temp", value: (weather.tempMin + "°"))
                    Spacer()
                    WeatherRow(logo: "thermometer.high", name: "Max temp", value: (weather.tempMax + "°"))
                        .padding(.trailing,8)
                }
                HStack{
                    WeatherRow(logo: "wind.snow", name: "Wind Speed", value: (String(format: "%0.1f", weather.windSpeed) + "m/s"))
                    Spacer()
                    WeatherRow(logo: "humidity", name: "Humidity", value: (String(format: "%0.0f", weather.humidity) + "%"))
                }
                
            }
            .frame(maxWidth: .infinity, alignment: .leading)
            .padding()
            
            
        }
        
        .foregroundColor(Color(hue: 0.65, saturation: 0.78, brightness: 0.35))
        .cornerRadius(20)
        
    }
}
