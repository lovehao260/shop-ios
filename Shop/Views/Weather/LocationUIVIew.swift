//
//  LocationUIVIew.swift
//  Shop
//
//  Created by CALC Company on 07/03/2023.
//

import Foundation
import SwiftUI
import MapKit
import CoreLocationUI

struct LocationUIView: View{
    @StateObject var viewModel = WeatherViewModel()
    
    @Binding var isShowingMapModal: Bool
    
    @Binding var selectedCoordinate: CLLocationCoordinate2D
    var body: some View{
        ZStack(alignment: .bottom){
            
            Map(coordinateRegion: $viewModel.region, showsUserLocation: true)
                .edgesIgnoringSafeArea(.all)
                .accentColor(Color(.systemPink))
                .gesture(
                    MagnificationGesture()
                        .onChanged { value in
                            let delta = 1.0 - value.magnitude
                            var span = viewModel.region.span
                            span.latitudeDelta *= delta
                            span.longitudeDelta *= delta
                            viewModel.region.span = span
                        }
                )
                .onAppear{
                    viewModel.checkIfLocationServiceIsEnabled()
                }
            
            Button(){
                self.isShowingMapModal.toggle()
                selectedCoordinate = viewModel.region.center
                
            }label: {
                Text("BAck")
            }
            LocationButton(.currentLocation) {
                // Fetch location with Core Location.
                viewModel.checkIfLocationServiceIsEnabled()
            }
            .foregroundColor(.white)
            .cornerRadius(8)
            .labelStyle(.titleAndIcon)
            .symbolVariant(.fill)
            .tint(.pink)
            .padding(.bottom, 50)
        }
    }
}

//struct LocationUIView_Previews: PreviewProvider{
//    static var previews: some View{
//        LocationUIView()
//    }
//}


