//
//  SpotifyUIView.swift
//  Shop
//
//  Created by CALC Company on 13/03/2023.
//

import SwiftUI

struct SpotifyUIView: View {
    
    @StateObject var songViewModel: SongViewModel = SongViewModel()
    var body: some View {
        ZStack {
            
            Color.primary
            Image(songViewModel.currenSong.imageName)
                .resizable()
                .blur(radius: 15)
                .opacity(0.7)
                .background(.thinMaterial)
            
            VStack {
                
                HStack {
                    TopButtonView(actionPrint: "Action", image: "chevron.down")
                        .foregroundColor(.white)
                    
                    Spacer()
                    
                    Text("EK Rock")
                        .foregroundStyle(.white)
                    
                    Spacer()
                    
                    TopButtonView(actionPrint: "More", image: "ellipsis")
                        .foregroundColor(.white)
                    
                }.padding()
                    .padding(.top, 40)
                
                
                Spacer()
                
                TabView(selection: $songViewModel.currenSong) {
                    
                    ForEach(songViewModel.songs) { song in
                        Image(songViewModel.currenSong.imageName)
                            .resizable()
                            .frame(maxWidth: 400, maxHeight: 500, alignment: .center)
                            .padding()
                            .tag(song)
                    }
                    
                }.tabViewStyle(.page(indexDisplayMode: .never))
                
                HStack {
                    VStack(alignment: .leading) {
                        Text(songViewModel.currenSong.title)
                            .font(.title.bold())
                            .foregroundStyle(.white)
                        Text(songViewModel.currenSong.artist)
                            .foregroundStyle(.regularMaterial)
                    }
                    
                    Spacer()
                    
                    Button {
                        print("More")
                    } label: {
                        Image(systemName: "heart.fill")
                            .font(.title)
                            .foregroundStyle(.green)
                    }
                }.padding()
                
                Slider(
                    value: $songViewModel.currentTime,
                    in: 0...songViewModel.audioDuration,
                    onEditingChanged: { editing in
                        if editing {
                            songViewModel.pauseSong()
                            songViewModel.isUpdatingCurrentTime = false
                        }else{
                            songViewModel.playSong()
                            songViewModel.isUpdatingCurrentTime = true
                        }
                    },
                    minimumValueLabel: Text("\(songViewModel.formatTime(songViewModel.currentTime))"),
                    maximumValueLabel: Text("\(songViewModel.formatTime(songViewModel.audioDuration))"),
                    label: { EmptyView() }
                )  .foregroundColor(.white)
                    .padding()
                    .onChange(of: songViewModel.currentTime) { newValue in
                        if !songViewModel.isUpdatingCurrentTime {
                            songViewModel.audioPlayer?.currentTime = newValue
                        }
                    }
                
                
                HStack {
                    Button {
                        print("Shuffle")
                    } label: {
                        Image(systemName: "shuffle")
                            .font(.title)
                    }
                    .foregroundStyle(.white)
                    
                    Spacer()
                    
                    Button {
                        withAnimation {
                            songViewModel.previousSong()
                        }
                    } label: {
                        Image(systemName: "backward.end.fill")
                            .font(.system(size: 35))
                    }
                    .foregroundStyle(.white)
                    
                    
                    Spacer()
                    
                    Button {
                        if songViewModel.audioPlayer.isPlaying{
                            songViewModel.pauseSong()
                        }else{
                            songViewModel.playSong()
                        }
                        
                    } label: {
                        Image(systemName:songViewModel.audioPlayer.isPlaying ? "pause.circle.fill" :"play.circle.fill")
                            .font(.system(size: 70))
                    }
                    .foregroundStyle(.white)
                    
                    
                    Spacer()
                    
                    Button {
                        withAnimation {
                            songViewModel.nextSong()
                        }
                        
                    } label: {
                        Image(systemName: "forward.end.fill")
                            .font(.system(size: 35))
                    }
                    .foregroundStyle(.white)
                    
                    
                    Spacer()
                    
                    Button {
                        print("More")
                    } label: {
                        Image(systemName: "repeat")
                            .font(.title)
                    }
                    .foregroundStyle(.white)
                    
                }
                .padding(.bottom, 100)
                .padding(.horizontal)
                
                Spacer()
                Spacer()
                
                
            }
            
        }.ignoresSafeArea()
            .onChange(of: songViewModel.currenSong) { newValue in
                print(newValue)
            }
        
    }
}

struct SpotifyUIView_Previews: PreviewProvider {
    static var previews: some View {
        SpotifyUIView()
    }
}

struct BackgroundColor: View{
    var body: some View {
        Color.primary
        Image("song")
            .resizable()
            .blur(radius: 20)
            .opacity(0.7)
            .background(.thinMaterial)
    }
}

struct TopButtonView: View {
    var actionPrint: String
    var image: String
    
    var body: some View {
        Button {
            print(actionPrint)
        } label: {
            Image(systemName: image)
        }
    }
}
