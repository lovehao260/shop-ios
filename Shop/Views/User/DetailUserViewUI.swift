//
//  DetailCatViewUI.swift
//  Shop
//
//  Created by CALC Company on 21/02/2023.
//

import SwiftUI

struct DetailUserViewUI: View {
    @Environment(\.presentationMode) var presentationMode
    @ObservedObject var accountListVM = AccountListViewModel()
    let item: Account
    @State var formData = FormData()
    @State private var selectedImage: UIImage?
    
    var body: some View {
        NavigationView{
            ZStack{
                GeometryReader { geo in
                    ZStack(alignment: .top) {
                        Image("background")
                            .resizable()
                            .scaledToFill()
                            .frame(width: geo.size.width, height: 300)
                            .edgesIgnoringSafeArea(.all)
                        ImagePickerView(selectedImage: $selectedImage, srcImage: $formData.avatar)
                            .offset(y: UIScreen.main.bounds.height / 4 - 150)
                    }
                }
                
                VStack(spacing: 10){
                    
                    TextField("Name", text: $formData.name)
                        .padding()
                        .background(Color.white)
                        .cornerRadius(6)
                        .padding(.bottom)
                    TextField("Username", text: $formData.username)
                        .padding()
                        .background(Color.white)
                        .cornerRadius(6)
                        .padding(.bottom)
                    TextField("Email", text: $formData.email)
                        .padding()
                        .background(Color.white)
                        .cornerRadius(6)
                        .padding(.bottom)
                    
                }
                .padding()
                .padding(.top, 20)
                .onAppear(perform: {
                    formData.name = item.name
                    formData.email = item.email
                    formData.username = item.username
                    formData.avatar = item.avatar
                })
                .alert(isPresented: $accountListVM.errors) {
                    Alert(title: Text("Error"), message: Text("Please Check data imput "), dismissButton: .default(Text("OK")))
                }
            }
            .background(Color.gray)
        }
        .navigationBarItems( trailing: trading)
        .accentColor(.black)
    }
    
    
    var trading:some View{
        Button(action: {
            validateUserUpdate(nameU:formData.name, emailU:formData.email ,usernameU: formData.username)
            accountListVM.getAllAccounts()
        }, label:{
            Text("Update")
        })
    }
    
    func validateUserUpdate(nameU:String, emailU:String, usernameU:String){
        if (isValidNull(nameU) && isValidNull(emailU) && isValidNull(usernameU)){
            if isValidEmail(emailU){
                accountListVM.handlerUpdateUser(id: item.id, formData:formData,selectedImage: selectedImage);
              
                DispatchQueue.main.async {
                   
                    presentationMode.wrappedValue.dismiss()
                }
                
            }
        }
    }
}
struct FormData {
    var name: String = ""
    var username: String = ""
    var email: String = ""
    var avatar: String = ""
}

//struct DetailCatViewUI_Previews: PreviewProvider {
//    static var previews: some View {
//        DetailCatViewUI()
//    }
//}
