//
//  CartUIView.swift
//  Shop
//
//  Created by CALC Company on 17/02/2023.
//

import SwiftUI
import Foundation


struct UserUIView: View {
    var body: some View {
        UserView()
    }
}

struct UserView: View{
    @StateObject  var accountListVM = AccountListViewModel()
    //    @ObservedObject var accountListVM = CatModel()
    @State var isPresentedNewPost = false
    @State var name = " "
    @State var username = " "
    @State var email = " "
    @State var password = " "
    @State var passwordConfirm = " "

    var body:some View{
        NavigationView{
            List {
                ForEach(accountListVM.items, id: \.id) {  item in
                    NavigationLink(
                        destination:DetailUserViewUI(item: item),
                        label: {
                            HStack(){
                                VStack {
                                    AsyncImage(
                                        url: URL(string: item.avatar),
                                        content: { image in
                                            image.resizable()
                                                .scaledToFit()
                                            
                                                .frame(maxWidth: 70, maxHeight: 100)
                                        },
                                        placeholder: {
                                            ProgressView()
                                        }
                                    )
                                }
                                VStack(alignment: .leading){
                                    Text(item.name)
                                    Text(item.email).font(.caption)
                                        .foregroundColor(.gray)
                                }
                                
                            }
                        }
                        
                    )
                }.onDelete(perform: deleteUser)
            }
            .listStyle(InsetListStyle())
            .navigationBarTitle("List Users")
            .navigationBarItems( trailing: plusButton)
        }.sheet(isPresented: $isPresentedNewPost, content:{
            NewUserUIview(isPresented: $isPresentedNewPost)
        }).onAppear {
            accountListVM.getAllAccounts()
        }
        
    }
    
    private func deleteUser(indexSet:IndexSet){
        let id = indexSet.map{accountListVM.items[$0].id}
        DispatchQueue.main.async {
            accountListVM.handlerDeleteUser(id: id[0])
       
        }
        
    }
    var plusButton:some View{
        
        Button(action: {
            isPresentedNewPost.toggle()
        }, label: {
            Image(systemName: "plus")
        })
    }
}



struct CartUIView_Previews: PreviewProvider {
    static var previews: some View {
        UserUIView()
    }
}

