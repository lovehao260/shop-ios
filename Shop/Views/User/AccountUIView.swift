//
//  AccountUIView.swift
//  Shop
//
//  Created by CALC Company on 05/06/2023.
//

import SwiftUI

struct AccountUIView: View {
    @ObservedObject var accountListVM = AccountListViewModel()
   
    var body: some View {
        NavigationStack{
            List {
                Section{
                    HStack{
                        AsyncImage(
                            url: URL(string: accountListVM.user.avatar),
                            content: { image in
                                image.resizable()
                                    .scaledToFit()
                                    .clipShape(Circle())
                                    .frame(width: 90, height: 90)
                            },
                            placeholder: {
                                ProgressView()
                            }
                        )
                
                        VStack(alignment: .leading, spacing: 6){
                            Text(accountListVM.user.name)
                                .fontWeight(.semibold)
                                .padding(.top,4)
                            
                            Text(accountListVM.user.email)
                                .font(.footnote)
                                .accentColor(.gray)
                            
                            
                        }
                    }
                }
                Section("General"){
                    HStack{
                        SettingRowView(imageName: "gear", title: "Version", tintColor: Color(.systemGray3))
                        Spacer()
                        Text("1.0.0")
                            .font(.subheadline)
                            .foregroundColor(.gray)
                    }
                }
                Section("Account"){
                    Button{
                      accountListVM.signout()
                    
                    }label: {
                        SettingRowView(imageName: "arrow.left.circle.fill", title: "Logout", tintColor: Color(.red))
                    }
                    
                    Button{
                        
                    }label: {
                        SettingRowView(imageName: "xmark.circle.fill", title: "Delete Account", tintColor: Color(.red))
                    }
                }
            }
            .onAppear {
                accountListVM.getMe()
             }
        }
        .navigationDestination(isPresented: $accountListVM.isLogout) {
            LoginUIView()
        }
    }
}

//struct AccountUIView_Previews: PreviewProvider {
//    static var previews: some View {
//        AccountUIView()
//    }
//}
