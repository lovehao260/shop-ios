//
//  NewUIView.swift
//  Shop
//
//  Created by CALC Company on 06/06/2023.
//

import SwiftUI

struct NewUIView: View {
    @State var animate = false
    @State private var date = Date()
    var body: some View {
        ZStack{
            Circle().fill(Color.green)
                .blur(radius: animate ? 30 : 100)
                          .frame(width: 200, height: 200)
                          .offset(x: animate ? -50: -130, y: animate ? -30 : -100)
            
            Circle().fill(Color.pink)
                .blur(radius: animate ? 30 : 100)
                          .frame(width: 300, height: 250)
                          .offset(x: animate ? 100: 130, y: animate ? 150 : 100 )
            
            VStack{
                Text("Admission notification")
                    .font(.title3)
                    .foregroundColor(.white)
                    .fontWeight(.bold)
                CustomSearchBar()
                
                DatePickerTextField(selectedDate: $date)
                               .padding()
                           Text("Selected Date: \(date)")
            }
            .padding(.horizontal, 20)
            .frame(maxWidth: .infinity, maxHeight: .infinity , alignment: .top)
        }
        .background(
            LinearGradient(gradient: Gradient(colors: [Color("backgroundColor"),Color("backgroundColor1")]), startPoint: .top, endPoint: .bottom)
          
        )
    }
    
    func ListNews()->some View{
        Text("sss")
    }
}

struct NewUIView_Previews: PreviewProvider {
    static var previews: some View {
        NewUIView()
    }
}
