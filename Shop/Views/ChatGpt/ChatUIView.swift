//
//  ChatUIView.swift
//  Shop
//
//  Created by CALC Company on 30/05/2023.
//

import SwiftUI
import Combine

struct ChatUIView: View {
    @State var chatMessages: [ChatMessage] = []
    @State var messageText : String = ""
    let openAiService = OpenAiService()
    @State var cancellables = Set<AnyCancellable>()
    @State var loadingChat: Bool = false
    @State private var isScrollNeeded: Bool = false
    var body: some View {
        VStack{
            ScrollViewReader { scrollView in
                ScrollView {
                    LazyVStack{
                        ForEach (chatMessages, id:  \.id){ message in
                            messageView(message: message)
                            
                        }
                    }
                }
                .onChange(of: isScrollNeeded) { _ in
                    // Scroll to the bottom whenever the messages change
                    scrollView.scrollTo(chatMessages.count - 1, anchor: .bottom)
                }
            }
    
            if (loadingChat){
                HStack {
                    DotView() // 1.
                    DotView(delay: 0.2) // 2.
                    DotView(delay: 0.4) // 3.
                }
            }
            
            HStack{
                TextField("Enter a message", text: $messageText)
                    .padding()
                    .background(.gray.opacity(0.1))
                    .cornerRadius(12)
                
                Button(action: {
            
                    sendMessage()
                }) {
                    Text("Send")
                        .foregroundColor(.white)
                        .padding()
                        .background(.black)
                        .cornerRadius(12)
                    
                }
            }
        }
        .padding()
        .onAppear {
            openAiService.sendMessage(message: "Generate a tagline for an ice cream shop")
        }
        
    }
    
    func messageView(message: ChatMessage)-> some View{
        HStack{
            if message.sender == .me {Spacer()}
            Text(message.content)
                .foregroundColor(message.sender == .me ? .white : .black)
                .padding()
                .background(message.sender == .me ? .blue : .gray.opacity(0.1))
                .cornerRadius(16)
                .frame(maxWidth:300, alignment: message.sender == .me ? .trailing : .leading)
            if message.sender == .gpt {Spacer()}
        }
    }
    
    func sendMessage(){
        loadingChat = true
        let myMessage  = ChatMessage(id: UUID().uuidString, content: messageText, dateCreated: Date(), sender: .me)
        chatMessages.append(myMessage)
     
        openAiService.sendMessage(message: messageText).sink { completion in
            // Handle error
        } receiveValue: { response in
            guard
                let textResponse = response.choices.first?.text
                    .trimmingCharacters(
                        in: .whitespacesAndNewlines
                            .union(.init(charactersIn: "\""))
                    )
            else { return }
            let gptMessage = ChatMessage(
                id: response.id,
                content: textResponse,
                dateCreated: Date(),
                sender: .gpt
            )
            chatMessages.append(gptMessage);
      
            DispatchQueue.main.async {
                      withAnimation {
                          loadingChat = false
                          isScrollNeeded = true
                      }
                  }
        }
        .store(in: &cancellables)
        isScrollNeeded = false
        messageText = ""
    }
}

struct ChatUIView_Previews: PreviewProvider {
    static var previews: some View {
        ChatUIView()
    }
}

struct ChatMessage {
    let id: String
    let content: String
    let dateCreated: Date
    let sender : MessageSender
}

enum MessageSender {
    case me
    case gpt
}

struct DotView: View {
    @State var delay: Double = 0 // 1.
    @State var scale: CGFloat = 0.1
    var body: some View {
        Circle()
            .frame(width: 20, height: 30)
            .scaleEffect(scale)
            .foregroundColor(.black.opacity(0.5))
            .animation(Animation.easeInOut(duration: 0.6).repeatForever().delay(delay)) // 2.
            .onAppear {
                withAnimation {
                    self.scale = 0.3
                }
            }
    }
}
