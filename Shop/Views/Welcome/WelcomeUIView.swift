
//
//  WelcomeUIView.swift
//  Shop
//
//  Created by CALC Company on 17/04/2023.
//

import SwiftUI
import Combine

struct WelcomeUIView: View {
    @State private var isPresented:Bool = false
    @Binding var isFirstLaunch: Bool
    let welcomeMessages: [WelcomeMessage] = [
        WelcomeMessage(title: "Hello",description: "Welcome to our application! This is a product designed to help you perform tasks easily and quickly."),
        WelcomeMessage(title: "Yamanashi",description: "This is a software for communication between students and teachers for Yamanashi schools."),
        WelcomeMessage(title: "Login",description: "To use the software, please log in to your account here.")
    ]
    @State private var offset: CGFloat = UIScreen.screenHeight

    var body: some View {
        NavigationStack{
            ZStack{
                WelcomeShape()
                TabView{
                    ForEach(welcomeMessages, id:\.self){ welcomeMessage in
                        ZStack{
                            VStack(alignment: .leading, spacing: 20) {
                                Text(welcomeMessage.title)
                                    .font(.system(size: 45))
                                    .foregroundColor(.white)
                                    .fontWeight(.bold)
                                Text(welcomeMessage.description)
                                    .font(.system(size: 23))
                                    .foregroundColor(.white)
                                    .fontWeight(.bold)
                                
                                if welcomeMessages.last == welcomeMessage {
                                    Button(action: {
                                        self.isPresented = true
                                        isFirstLaunch = false
                                    }) {
                                        Text ("Login")
                                            .frame(minWidth: 0,maxWidth: UIScreen.screenWidth/2)
                                            .font(.system(size: 18))
                                            .padding()
                                            .foregroundColor(.white)
                                            .overlay(
                                                RoundedRectangle(cornerRadius: 25) .stroke(Color.white, lineWidth: 2))
                                            .background(Color.blue)
                                            .cornerRadius(25)
                                            .offset(x:0.0, y: offset)
                                        
                                    }
                                }
                                
                            }
                            .padding(20)
                            .onAppear{
                                self.offset = UIScreen.screenHeight
                                if welcomeMessages.last == welcomeMessage {
                                    withAnimation(.linear( duration: 1)) {
                                        self.offset = 0
                                    }
                                }
                                
                            }
                        }
                    }
                }
                .frame(width: UIScreen.screenWidth, height: UIScreen.screenHeight, alignment: .center)
                .tabViewStyle(PageTabViewStyle())
                
                
            }
            .navigationDestination(isPresented: $isPresented) {
                LoginUIView()
            }
        }
    
    }
     
}

struct WelcomeUIView_Previews: PreviewProvider {
    static var previews: some View {
        WelcomeUIView(isFirstLaunch: .constant(true))
    }
}
