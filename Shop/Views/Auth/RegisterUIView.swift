//
//  RegisterUIView.swift
//  Shop
//
//  Created by CALC Company on 16/02/2023.
//

import SwiftUI

struct RegisterUIView: View {
    
    @State private var wrongName = 0;
    @State private var wrongEmail = 0;
    @State private var wrongUsername = 0;
    @State private var wrongPassword = 0;
    @State private var wrongPasswordConfirm = 0;
    @State private var showingLonginScreen = false;
    @State private var isPasswordHidden = true
    
    @ObservedObject var registerVM = LoginViewModel()
    
    @State var formData = DataRegister()
    
    var body: some View {
        
        NavigationStack{
            ZStack{
                Color.blue.ignoresSafeArea()
                Circle().scale(1.7)
                    .foregroundColor(.white.opacity(0.15))
                Circle().scale(1.5)
                    .foregroundColor(.white)
                
                VStack{
                    // logo
                    VStack {
                        Image("logo")
                            .resizable()
                            .frame(width: 350, height:0)
                            .clipShape(Circle())
                        VStack(alignment: .leading) {
                            Text("CALC Register")
                                .font(.largeTitle)
                                .fontWeight(.bold)
                        }
                    }
                    TextField("Name",text: $formData.name)
                        .padding()
                        .frame(width: 350, height:50)
                        .background(Color.black.opacity(0.05))
                        .cornerRadius(10)
                        .border(.red, width: CGFloat(wrongName))
                    
                    TextField("email",text:  $formData.email)
                        .autocapitalization(.none)
                        .padding()
                        .frame(width: 350, height:50)
                        .background(Color.black.opacity(0.05))
                        .cornerRadius(10)
                        .border(.red, width: CGFloat(wrongEmail))
                    
                    TextField("username",text: $formData.username)
                        .autocapitalization(.none)
                        .padding()
                        .frame(width: 350, height:50)
                        .background(Color.black.opacity(0.05))
                        .cornerRadius(10)
                        .border(.red, width: CGFloat(wrongUsername))
                    
                    SecureField("password",text: $formData.password)
                        .autocapitalization(.none)
                        .padding()
                        .frame(width: 350, height:50)
                        .background(Color.black.opacity(0.05))
                        .cornerRadius(10)
                        .border(.red, width: CGFloat(wrongPassword))
                    
                    SecureField("passwordConfirm",text:  $formData.confirmPassword)
                        .autocapitalization(.none)
                        .padding()
                        .frame(width: 350, height:50)
                        .background(Color.black.opacity(0.05))
                        .cornerRadius(10)
                        .border(.red, width: CGFloat(wrongPassword))
                    
                    VStack{
                        CustomTextField(placeHolder: "name", imageName: "eye", bColor: "red", tOpacity: 0.7, value: $formData.name)
                    }
                    
                    Button("Register"){
                        validateUser()
                    }
                    
                    .foregroundColor(.white)
                    .frame(width: 300,height: 50)
                    .background(Color.blue)
                    .cornerRadius(10)
                    
                    VStack {
                        NavigationLink(destination: LoginUIView()) {
                            Text("You have an account? ")
                                .foregroundColor(.gray) +
                            Text("Sign In")
                                .foregroundColor(.blue)
                        }
                        .padding()
                        .frame(width: 450, height:50)
                        
                    }
                }
                .alert(isPresented: $registerVM.showErrorAlert) {
                    Alert(title: Text("Error"), message: Text("Can't register app, Please check your username or email exits"), dismissButton: .default(Text("OK")))
                }
                
            }
            .navigationBarHidden(true )
            .navigationDestination(isPresented: $registerVM.showingLonginScreen) {
                LoginUIView()
            }
            
        }
    }
    
    func validateUser(){
        if isValidEmail(formData.email){
            wrongEmail = 0;
        } else {
            wrongEmail = 2;
        }
        
        if (isValidNull(formData.name)){
            wrongName = 0;
        } else {
            wrongName = 2;
        }
        
        if (isValidNull(formData.username)){
            wrongUsername = 0;
        }else{
            wrongUsername = 2;
        }
        if (isValidNull(formData.password)){
            wrongPassword = 0;
            
            if passwordsMatch(password: formData.password, confirmPassword: formData.confirmPassword){
                if (isValidInputLength(formData.password, min: 8, max: 20)){
                    wrongPassword = 0;
                    registerVM.registerHandler(formData: formData)
                  

                }else{
                    wrongPassword = 2;
                }
                
            }else{
                wrongPassword = 2;
            }
            
        }else{
            wrongPassword = 2;
        }
    }
}

struct RegisterUIView_Previews: PreviewProvider {
    static var previews: some View {
        RegisterUIView()
    }
}
