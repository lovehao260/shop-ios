
import SwiftUI

struct ForgotOtpUIView: View {
    @Environment(\.presentationMode) var present
    @ObservedObject var viewModel: ForgotPasswordViewModel
    
    @State var SendPass = false;
    @State  var emailForgot = Forgot()
    @State var ErrorOtp = false;
    @State private var showButton = true
    @State var numberVerify = 5
    @State var disableVerify = false
    
    let timer = Timer.publish(every: 1, on: .main, in: .common).autoconnect()
    var body: some View {
        NavigationStack{
            VStack{
                VStack{
                    VStack{
                        HStack{
                            Button(action: {
                                present.wrappedValue.dismiss()
                            }, label: {
                                Image (systemName: "arrow.left")
                                    .font(.title2)
                                    .foregroundColor(.black)
                            })
                            Spacer()
                            Text("Verify OTP")
                                .font(.title2)
                                .fontWeight(.bold)
                                .foregroundColor(.black)
                            
                            Spacer()
                        }
                        .padding()
                        
                        Text("Code send to email:\(viewModel.DataForgot.email)")
                            .foregroundColor(.gray)
                            .padding()
                        
                        Spacer(minLength: 0)
                        HStack{
                            ForEach(0..<6,id: \.self){ index in
                                CodeView(code: getCodeAtIndex(index: index), ErrorOtp: ErrorOtp)
                            }
                            
                        }
                        .padding()
                        .padding(.horizontal, 20)
                        if ErrorOtp{
                            Text("The authentication code is incorrect, the number of attempts remaining is \(numberVerify)")
                                .foregroundColor(.red)
                                .padding()
                        }
                        
                        
                        Spacer()
                        
                        HStack(spacing: 6){
                            if showButton {
                                Text("Did't receive code?")
                                    .foregroundColor(.black)
                                Button (action: {
                                    showButton = false
                                    viewModel.code = ""
                                    emailForgot.email = viewModel.DataForgot.email
                                    viewModel.forgotPasswordEmail(emailForgot:emailForgot)
                                }, label:{
                                    Text("Resend")
                                        .fontWeight(.bold)
                                        .foregroundColor(.black)
                                        .padding(.vertical)
                                    
                                })
                                .disabled(disableVerify)
                            } else {
                                Text("Wait for 5 seconds...")
                                    .onAppear {
                                        Timer.scheduledTimer(withTimeInterval: 5, repeats: false) { _ in
                                            showButton = true
                                        }
                                    }
                            }
                            
                        }
                        
                        Button (action: {
                            
                            CheckVerifyToken(code: viewModel.code, otpCode:viewModel.DataForgot.otp)
                        }, label:{
                            Text("Verify code and change password")
                                .fontWeight(.bold)
                                .padding(.vertical)
                                .frame(width: UIScreen.main.bounds.width - 30)
                                .foregroundColor(.white)
                                .background(.blue)
                            
                        }).padding()
                            .disabled(disableVerify)
                        
                        
                    }
                    .frame(height: UIScreen.main.bounds.height / 1.7)
                    .background(.white)
                    .cornerRadius(20)
                    
                    Spacer()
                    
                    VStack{
                        CustomNumberPad(value: $viewModel.code, isVerify: true)
                    }.background(Color.gray.ignoresSafeArea(.all, edges: .bottom))
                }
                .navigationBarHidden(true)
                .navigationBarBackButtonHidden(true)
            }
            .navigationDestination(isPresented: $SendPass) {
                ChangePaswordUIView(viewModel: viewModel)
            }
            .alert(isPresented: $disableVerify) {
                Alert(title: Text("Error"), message: Text("Please go back to the authentication screen and verify again !!"), dismissButton: .default(Text("OK")))
            }
            
        }
    }
    // geting code at each index
    func getCodeAtIndex(index: Int)->String{
        if viewModel.code.count > index{
            let start = viewModel.code.startIndex
            let current = viewModel.code.index(start, offsetBy: index)
            return String(viewModel.code[current])
        }
        return ""
    }
    
    func CheckVerifyToken(code: String,otpCode: String){
        if code.count == 6{
            if (otpCode == viewModel.code){
                self.SendPass = true
                viewModel.code = ""
                self.ErrorOtp = false
                
            }else{
                if  self.numberVerify > 0{
                    self.numberVerify -= 1
                }
                self.ErrorOtp = true
            }
        }
        if  self.numberVerify > 0{
            self.numberVerify -= 1
            self.ErrorOtp = true
        }else if self.numberVerify == 0{
            
            disableVerify.toggle()
            
        }
        
    }
}

struct CodeView: View{
    var code: String
    var ErrorOtp: Bool
    var body: some View{
        VStack(spacing: 10){
            Text(code)
                .foregroundColor( ErrorOtp ? .red : .black)
                .fontWeight(.bold)
                .font(.title2)
                .frame(height: 45)
            
            Capsule()
                .fill(ErrorOtp ? Color.red.opacity(0.5) : Color.gray.opacity(0.5))
                .frame(height: 4)
        }
    }
}
//struct ForgotOtpUIView_Previews: PreviewProvider {
//    
//    static var previews: some View {
//        ForgotOtpUIView(viewModel: ForgotPasswordViewModel())
//    }
//}
