//
//  LoginUIView.swift
//  Shop
//
//  Created by CALC Company on 16/02/2023.
//

import SwiftUI

struct LoginUIView: View {
    @ObservedObject var loginVM = LoginViewModel()
    @StateObject private var accountListVM = AccountListViewModel()


    @State private var showingLonginScreen = false;
    @State private var isPasswordHidden = true
 
    private let button:UIButton = {
        let button = UIButton()
        button.backgroundColor = .systemRed
        button.setTitle("SignUp", for: .normal)
        return button;
    }()
    var body: some View {
        
        NavigationStack{
            ZStack{
                Color.blue.ignoresSafeArea()
                Circle().scale(1.7)
                    .foregroundColor(.white.opacity(0.15))
                Circle().scale(1.35)
                    .foregroundColor(.white)
                
                VStack{
                    // logo
                    VStack {
                        Image("logo")
                            .resizable()
                            .frame(width: 350, height:50)
                            .clipShape(Circle())
                        VStack(alignment: .leading) {
                            Text("CALC Login")
                                .font(.largeTitle)
                                .fontWeight(.bold)
                            
                        }
                    }
                    TextField("username",text: $loginVM.credentials.username)
                        .padding()
                        .autocapitalization(.none)
                        .frame(width: 350, height:50)
                        .background(Color.black.opacity(0.05))
                        .cornerRadius(10)
//                        .border(.red, width: CGFloat($loginVM.wrongUsername))
                    
                    if isPasswordHidden {
                        SecureField("password",text: $loginVM.credentials.password)
                            .overlay(
                                HStack {
                                    Spacer()
                                    Image(systemName: isPasswordHidden ? "eye.slash" : "eye")
                                        .foregroundColor(.gray)
                                        .onTapGesture {
                                            isPasswordHidden.toggle()
                                        }
                                    
                                }
                            )
                            .autocapitalization(.none)
                            .padding()
                            .frame(width: 350, height:50)
                            .background(Color.black.opacity(0.05))
                            .cornerRadius(10)
//                            .border(.red, width: CGFloat($loginVM.wrongPassword))
                    } else {
                        TextField("password",text:  $loginVM.credentials.password)
                            .overlay(
                                HStack {
                                    Spacer()
                                    Image(systemName: isPasswordHidden ? "eye.slash" : "eye")
                                        .foregroundColor(.gray)
                                        .onTapGesture {
                                            isPasswordHidden.toggle()
                                        }
                                    
                                }
                            )
                            .autocapitalization(.none)
                            .padding()
                            .frame(width: 350, height:50)
                            .background(Color.black.opacity(0.05))
                            .cornerRadius(10)
               
                    }
                    
                    
                    HStack {
                        Spacer()
                        NavigationLink(destination: ForgotPasswordUIView()) {
                            Text("Forgot password ?")
                        }
                        .padding()
                        .frame(width: 230, height:50)
                    
                    }
                    
                    Button("Login"){
                        loginVM.login()
                  
                    }.disabled(loginVM.isLoginButtonDisabled)
                    
                    .foregroundColor(.white)
                    .frame(width: 300,height: 50)
                    .background(Color.blue)
                    .cornerRadius(10)
            
                    VStack {
                        
                        NavigationLink(destination: RegisterUIView()) {
                            Text("Don't have an account? ")
                                .foregroundColor(.gray) +
                            Text("Sign up")
                                .foregroundColor(.blue)
                        }
                        .padding()
                        .frame(width: 450, height:50)
                        
                    }
                }
                .alert(isPresented: $loginVM.showErrorAlert) {
                         Alert(title: Text("Error"), message: Text("Can't login app, Please check your username and password"), dismissButton: .default(Text("OK")))
                     }
            }
            
            .navigationBarHidden(true )
            .navigationDestination(isPresented: $loginVM.isAuthenticated) {
                HomeUIView()
            }

        }
    }
}

struct LoginUIView_Previews: PreviewProvider {
    static var previews: some View {
        LoginUIView()
    }
}
