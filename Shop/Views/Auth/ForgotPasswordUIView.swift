//
//  ForgotOtpUIView.swift
//  Shop
//
//  Created by CALC Company on 22/03/2023.
//

import SwiftUI

struct ForgotPasswordUIView: View {
    @ObservedObject var viewModel = ForgotPasswordViewModel()
    @State  var emailForgot = Forgot()
    @State var emailWrong = 0;

    var body: some View {
        
        NavigationStack{
            VStack{
                
                VStack{
                    Text("Send mail With OTP")
                        .font(.title2)
                        .fontWeight(.bold)
                        .padding()
                        .foregroundColor(.black)
                    Image("verify")
                        .resizable()
                        .aspectRatio(contentMode:.fit)
                        .padding()
                    Text("You'll receive a 6 digit code\n to veriry next.")
                        .font(.title2)
                        .foregroundColor(.gray)
                        .multilineTextAlignment(.center)
                        .padding()
                    // Email otp field
                    
                    HStack{
                        VStack(alignment: .leading, spacing: 6){
                            
                            TextField("email",text: $emailForgot.email)
                                .padding()
                                .autocapitalization(.none)
                                .frame(width: 280, height:50)
                                .background(Color.black.opacity(0.05))
                                .cornerRadius(10)
                                .border(.red, width: CGFloat(emailWrong))
                        }
                        
                        if viewModel.isLoading {
                            ProgressView()
                                .progressViewStyle(CircularProgressViewStyle())
                                .accentColor(.white)
                                .padding(.vertical, 15)
                                .padding(.horizontal, 25)
                                .foregroundColor(.white)
                                .background(.blue)
                                .cornerRadius(15)
                        }else{
                            Button (action: {
                            
                                sendMailForgot()
                            }) {
                                Image(systemName: "paperplane")
                                    .padding(.vertical, 15)
                                    .padding(.horizontal, 25)
                                    .foregroundColor(.white)
                                    .background(.blue)
                                    .cornerRadius(15)
                            }
                         
                        }
                        
                        
                    }
                    .padding()
                    .background(Color.white)
                    .cornerRadius(15)
                    .shadow(color: Color.black.opacity(0.1), radius: 2, x: 0, y: -5)
                }
                .frame(maxWidth: .infinity, minHeight: UIScreen.main.bounds.height / 1.8)
                .background(.white)
                .cornerRadius(20)
                VStack {
                    NavigationLink(destination: LoginUIView()) {
                        Text("You have an account? ")
                            .foregroundColor(.gray) +
                        Text("Sign In")
                            .foregroundColor(.blue)
                    }
                    .padding()
                    .frame(width: 450, height:50)
                    
                }
            }
            .alert(isPresented: $viewModel.showErrorAlert) {
                Alert(title: Text("Error"), message: Text(viewModel.errorMessage), dismissButton: .default(Text("OK")))
            }
            .navigationBarHidden(true)
            .navigationBarBackButtonHidden(true)
            .navigationDestination(isPresented: $viewModel.showingSheetOtp) {
                ForgotOtpUIView(viewModel: viewModel)
                
                //                ForgotOtpUIView()
            }
        }
    }
    func sendMailForgot(){
        if isValidEmail(emailForgot.email){
            emailWrong = 0
            viewModel.isLoading.toggle()
            viewModel.forgotPasswordEmail(emailForgot:emailForgot)
            
        }else{
            emailWrong = 2
        }
        
    }
}

struct ForgotPasswordUIView_Previews: PreviewProvider {
    static var previews: some View {
        ForgotPasswordUIView()
    }
}


