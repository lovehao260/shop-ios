//
//  ChangePaswordUIView.swift
//  Shop
//
//  Created by CALC Company on 22/03/2023.
//

import SwiftUI

struct ChangePaswordUIView: View {
    @State private var password: String = ""
    @State private var confirmPassword: String = ""
    
    @State private var isShowingAlert: Bool = false
    @State private var passwordWrong: Int = 0
    @ObservedObject var viewModel: ForgotPasswordViewModel
    
    var body: some View {
        NavigationStack{
            VStack{

                VStack{
                    Text("Change password")
                        .font(.title2)
                        .fontWeight(.bold)
                        .padding()
                        .foregroundColor(.black)
                    Image("verify")
                        .resizable()
                        .aspectRatio(contentMode:.fit)
                        .padding()
                    Text("You need to enter a password of at least 8 characters.")
                        .font(.title2)
                        .foregroundColor(.gray)
                        .multilineTextAlignment(.center)
                        .padding()
                    // Email otp field
                    
                    VStack(alignment: .leading, spacing: 6){
                        
                        SecureField("Password",text: $password)
                            .autocapitalization(.none)
                            .padding()
                            .frame(width: 350, height:50)
                            .background(Color.black.opacity(0.05))
                            .cornerRadius(10)
                            .overlay(
                                RoundedRectangle(cornerRadius: 5)
                                    .stroke(Color.red, lineWidth:CGFloat(passwordWrong) )
                         
                            )
                           // sets the accent color to red
                        
                        SecureField("Confirm Password",text: $confirmPassword)
                            .autocapitalization(.none)
                            .padding()
                            .frame(width: 350, height:50)
                            .background(Color.black.opacity(0.05))
                            .cornerRadius(10)
                            .overlay(
                                RoundedRectangle(cornerRadius: 5)
                                    .stroke(Color.red, lineWidth:CGFloat(passwordWrong) )
                         
                            )
                        
                        HStack{
                            Spacer()
                            Button (action: {
                                SendChangPassword()
                            }) {
                                HStack{
                                    Text("Update Password")
                                    Image(systemName: "paperplane")
                                }
                                .frame(width: 200)
                                .padding(.vertical, 15)
                                .padding(.horizontal, 25)
                                .foregroundColor(.white)
                                .background(.blue)
                                .cornerRadius(15)
                            }
                            Spacer()
                            
                        }.padding()
                        
                        
                    }
                    .padding()
                    .background(Color.white)
                    .cornerRadius(15)
                    .shadow(color: Color.black.opacity(0.1), radius: 2, x: 0, y: -5)
                }
                .frame(maxWidth: .infinity, minHeight: UIScreen.main.bounds.height / 1.8)
                .background(.white)
                .cornerRadius(20)
                VStack {
                    NavigationLink(destination: LoginUIView()) {
                        Text("You have an account? ")
                            .foregroundColor(.gray) +
                        Text("Sign In")
                            .foregroundColor(.blue)
                    }
                    .padding()
                    .frame(width: 450, height:50)
                    
                }
            }
            .navigationBarHidden(true)
            .navigationBarBackButtonHidden(true)
            .navigationDestination(isPresented: $viewModel.moveToLogin) {
                LoginUIView()
            }
        }
    }
    
    func SendChangPassword(){
        if (isValidNull(password)){
            passwordWrong = 0;
            
            if passwordsMatch(password: password, confirmPassword: confirmPassword){
                if (isValidInputLength(password, min: 8, max: 20)){
                    passwordWrong = 0;
              
                    let formFields = PasswordChang(id:String( viewModel.DataForgot.id), otp: viewModel.DataForgot.otp, password: password, password_confirmation: confirmPassword)
                    viewModel.changePassword(formFields:formFields)
                    
                }else{
                    passwordWrong = 2;
                }
                
            }else{
                passwordWrong = 2;
            }
            
        }else{
            passwordWrong = 2;
        }
    }
}
struct PasswordChang {
    var id: String
    var otp: String
    var password: String
    var password_confirmation: String
}
//struct ChangePaswordUIView_Previews: PreviewProvider {
//    static var previews: some View {
//        ChangePaswordUIView(viewModel: ForgotPasswordViewModel())
//    }
//}
