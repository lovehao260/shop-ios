//
//  HomeUIView.swift
//  Shop
//
//  Created by CALC Company on 16/02/2023.
//

import SwiftUI

struct HomeUIView: View {
    @State private var selectedTab = 0
    var body: some View {
        TabView(selection: $selectedTab) {
            TaskUIView()
                .tabItem {
                    Image(systemName: "house")
                    Text("Task")
                }
                .tag(7)
            SpotifyUIView()
                .tabItem {
                    Image(systemName: "music.note.list")
                    Text("Spotify")
                }
                .tag(1)
            WeatherUIView()
                .tabItem {
                    Image(systemName: "cloud.fog.fill")
                    Text("Weather")
                }
                .tag(2)
            UserUIView()
                .tabItem {
                    Image(systemName: "person")
                    Text("Users")
                }
                .tag(3)
            
            CalculatorUIView()
                .tabItem {
                    Image(systemName: "music.note.list")
                    Text("Calculator")
                }
                .tag(4)
            AccountUIView()
                .tabItem {
                    Image(systemName: "person")
                    Text("Account")
                }
                .tag(5)
            
            ChatUIView()
                .tabItem {
                    Image(systemName: "house")
                    Text("Spotify")
                }
                .tag(6)
        } .navigationBarHidden(true)
        
        
    }
    
    
}

struct HomeUIView_Previews: PreviewProvider {
    static var previews: some View {
        HomeUIView()
    }
}

