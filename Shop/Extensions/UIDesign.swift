//
//  UIDesign.swift
//  Shop
//
//  Created by CALC Company on 05/06/2023.
//

import SwiftUI
import Foundation
import UIKit

extension View {
    func hLeading() ->some View{
        self
            .frame(maxWidth: .infinity, alignment: .leading)
    }
    
    func hTrailing() ->some View{
        self
            .frame(maxWidth: .infinity, alignment: .trailing)
    }
    func hCenter() ->some View{
        self
            .frame(maxWidth: .infinity, alignment: .center)
    }
    // MARK: Safe area
    func getSafeArea()->UIEdgeInsets{
        guard let screen  = UIApplication.shared.connectedScenes.first as? UIWindowScene else {
            return .zero
        }
        guard let safeArea = screen.windows.first?.safeAreaInsets else {
            return .zero
        }
        
        return safeArea
    }
}
