//
//  UiScreen+Utilities.swift
//  Shop
//
//  Created by CALC Company on 17/04/2023.
//

import UIKit

extension UIScreen {
    static let screenWidth = UIScreen.main.bounds .size.width
    static let screenHeight = UIScreen.main.bounds.size.height
    static let screenSize = UIScreen.main.bounds.size
}
