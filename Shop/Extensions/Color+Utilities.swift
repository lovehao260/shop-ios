//
//  Color+Utilities.swift
//  Shop
//
//  Created by CALC Company on 18/04/2023.
//

import SwiftUI
extension Color{
    init(_ red: CGFloat , _ green: CGFloat, _ blue: CGFloat){
        self.init(red: red / 255.0, green: green / 255.0, blue: blue / 255.0)
    }
}
