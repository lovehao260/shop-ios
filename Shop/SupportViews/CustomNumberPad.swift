//
//  CustomNumberPad.swift
//  Shop
//
//  Created by CALC Company on 22/03/2023.
//

import Foundation
import SwiftUI

struct CustomNumberPad: View {
    @Binding var value: String
    var isVerify :Bool
    var placeHolder: String = ""
    var rows = ["1","2","3","4","5","6","7","8","9","","0","delete.left"]
    
    var body: some View {
        GeometryReader { reader in
            VStack{
                LazyVGrid(columns: Array(repeating: GridItem(.flexible(), spacing: 20), count: 3)){
                    ForEach (rows, id: \.self){ value in
                        Button(action: {
                            btnAction(value: value)
                        }, label: {
                            ZStack{
                                if value == "delete.left"{
                                    Image(systemName: value)
                                        .font(.title2)
                                        .foregroundColor(.black)
                                }else{
                                    Text(value)
                                        .font(.title2)
                                        .fontWeight(.bold)
                                        .foregroundColor(.black)
                                }
                            }
                            .frame(width: getWidth(frame: reader.frame(in: .global)), height: getHeight(frame: reader.frame(in: .global)))
                            .background(.white)
                            .cornerRadius(10)
                        })
                    }
                }
            }
            
        }.padding()
    }
    func getWidth(frame: CGRect)-> CGFloat{
        let width = frame.width
        let actuaWidth = width - 40
        return actuaWidth / 3
        
    }
    
    func getHeight(frame: CGRect)->CGFloat{
        let height = frame.height
        let actuaHeight = height - 30
        return actuaHeight / 4
        
    }
    func btnAction(value: String){
        if value == "delete.left" && self.value != ""
        {
            self.value.removeLast()
        }
        if value == "" && self.value != ""
        {
            self.value.removeAll()
    
        }
        if value != "delete.left" 
        {
            self.value.append(value)
        }
        
    }
}
