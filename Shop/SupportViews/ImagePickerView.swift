//
//  ImagePickerView.swift
//  Shop
//
//  Created by CALC Company on 03/03/2023.
//

import Foundation
import SwiftUI

struct ImagePickerView: View {
    @Binding var selectedImage: UIImage?
    @Binding var srcImage : String
    @State private var isShowingPhotoPicker = false
    @State private var isClicked = false
    @State private var selectedOption: String = ""
    
    var body: some View {
        ZStack(alignment: .bottomTrailing) {
            if let image = srcImage {
                AsyncImage(
                    url: URL(string: image),
                    content: { image in
                        image
                            .resizable()
                            .frame(maxWidth: 150, maxHeight: 150, alignment: .center)
                            .clipShape(Circle())
                            .overlay(Circle().stroke(Color.white, lineWidth: 4))
                            .shadow(radius: 5)
                    },
                    placeholder: {
                        ProgressView()
                    }
                )
                
            }
            if let photo = selectedImage {
                Image(uiImage: photo)
                    .resizable()
                    .frame(maxWidth: 150, maxHeight: 150, alignment: .center)
                    .clipShape(Circle())
                    .overlay(Circle().stroke(Color.white, lineWidth: 4))
                    .shadow(radius: 5)
               }
            ZStack(alignment: .bottomTrailing) {
                Color.white
                Image(systemName: "camera")
                    .foregroundColor(.black)
                    .frame(maxWidth: 50, maxHeight: 50)
                    .padding()
            }
            .clipShape(Circle())
            .overlay(Circle().stroke(Color.black, lineWidth: 2))
            .frame(width: 60, height: 50)
            
        } .gesture(TapGesture()
            .onEnded {
                self.isClicked.toggle()
            }
        )
        .sheet(isPresented: $isShowingPhotoPicker) {
            if self.selectedOption == "camera"{
                ImagePicker(selectedImage: $selectedImage, sourceType: .camera)
            }else{
                ImagePicker(selectedImage: $selectedImage, sourceType: .photoLibrary)
            }
          
         
        }
        .actionSheet(isPresented: $isClicked) {
            ActionSheet(title: Text("Options"), buttons: [
                .default(Text("Camera")) {
                    self.selectedOption = "camera"
                    self.isShowingPhotoPicker.toggle()
                },
                .default(Text("Libary")) {
                    self.selectedOption = "libary"
                    self.isShowingPhotoPicker.toggle()
                },
                .cancel()
            ])
        }
        
    }
}


struct ImagePicker: UIViewControllerRepresentable {
    @Binding var selectedImage: UIImage?
    @Environment(\.presentationMode) private var presentationMode
    var sourceType: UIImagePickerController.SourceType = .photoLibrary
    
    func makeUIViewController(context: Context) -> UIImagePickerController {
        let imagePicker = UIImagePickerController()
         imagePicker.allowsEditing = false
         imagePicker.sourceType = sourceType
         imagePicker.delegate = context.coordinator
         
         return imagePicker
    }
    
    func updateUIViewController(_ uiViewController: UIImagePickerController, context: Context) {
        // Nothing to update here
    }
    
    func makeCoordinator() -> Coordinator {
        Coordinator(selectedImage: $selectedImage)
    }
    
    class Coordinator: NSObject, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
        @Binding var selectedImage: UIImage?
        
        init(selectedImage: Binding<UIImage?>) {
            _selectedImage = selectedImage
        }
        
        func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
            guard let selectedImage = info[.originalImage] as? UIImage else {
                return
            }
            
            self.selectedImage = selectedImage
            picker.dismiss(animated: true, completion: nil)
        }
        
        func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
            picker.dismiss(animated: true, completion: nil)
        }
    }
}
