//
//  CustomTabBar.swift
//  Shop
//
//  Created by CALC Company on 17/03/2023.
//

import SwiftUI

struct CustomTabBar: View {
    @Binding var currentTab: Tab
    var body: some View {
        HStack(spacing: 0.0){
            ForEach(Tab.allCases, id: \.rawValue){ tab in
                Button(action: {
                    
                }, label: {
                    Image(systemName: tab.rawValue)
                        .renderingMode(.template)
                        .frame(maxWidth: .infinity)
                        .foregroundColor(.white)
                        .offset(y: currentTab == tab ? -17 : 0)
                })
            }
        }
        .frame(width: .infinity)
        .background(.red)
        
    }
}

struct CustomTabBar_Previews: PreviewProvider {
    static var previews: some View {
        CustomTabBar(currentTab: .constant(.home))
    }
}
