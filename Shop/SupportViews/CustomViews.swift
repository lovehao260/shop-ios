//
//  CustumViews.swift
//  Shop
//
//  Created by CALC Company on 17/02/2023.
//

import SwiftUI
struct CustomViews: View{
    var body: some View{
        Text("Hello")
    }
}

struct CustomView_Peviews{
    static var preview : some View{
        CustomViews()
    }
}

struct CustomTextField:View{
    var placeHolder:String
    var imageName: String
    var bColor:String
    var tOpacity: Double
    @Binding var value: String
    
    var body: some View{
        HStack{
            
            Image(systemName: imageName)
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(width: 20, height: 20)
                .foregroundColor(Color(bColor).opacity(tOpacity))
            if (placeHolder == "Password" || placeHolder == "Confirm Password"){
                ZStack(alignment: .leading){
                    if value.isEmpty{
                        Text(placeHolder)
                            .foregroundColor(Color(bColor).opacity(tOpacity))
                            .font(.system(size:20))
                    }
                    SecureField("", text: $value)
                        .padding(.leading,12)
                        .font(.system(size: 20))
                        .frame(height: 45)
                }
                
            }else{
                ZStack(alignment: .leading){
                    if value.isEmpty{
                        Text(placeHolder)
                            .foregroundColor(Color(bColor).opacity(tOpacity))
                            .padding(.leading,12)
                            .font(.system(size: 20))
                    }
                    
                    TextField("", text: $value)
                        .padding(.leading, 12)
                        .font(.system(size: 20))
                        .frame(height: 45)
                        .foregroundColor(Color(bColor))
                }
            }
        }
        .overlay(
            Divider()
                .overlay(
                    Color(bColor).opacity(tOpacity)
                )
                .padding(.horizontal,20),
            alignment: .bottom
        )
    }
}
