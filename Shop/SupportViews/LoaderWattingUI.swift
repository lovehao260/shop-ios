//
//  LoaderWattingUI.swift
//  Shop
//
//  Created by CALC Company on 23/03/2023.
//

import SwiftUI

struct LoaderWattingUI: View {
    @State var show  = true
    var body: some View {
        if self.show{
            GeometryReader { geometry in
                Spacer()
                ZStack{
                    
                    ProgressView()  .progressViewStyle(CircularProgressViewStyle())
                        .frame(width: 45, height: 45)
                        .padding()
                }.background(.white)
                    .cornerRadius(15)
                
            }.background(Color.gray.opacity(0.45).edgesIgnoringSafeArea(.all))
                .onTapGesture {
                    self.show.toggle()
                }
        }else{
            Text("sss")
        }
    }
}

struct LoaderWattingUI_Previews: PreviewProvider {
    static var previews: some View {
        LoaderWattingUI()
    }
}
