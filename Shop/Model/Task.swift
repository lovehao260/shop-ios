//
//  Task.swift
//  Shop
//
//  Created by CALC Company on 05/06/2023.
//

import Foundation

struct Task: Identifiable{
    let id = UUID().uuidString
    let taskTitle:String
    let taskDescription:String
    let taskDate: Date
}
