//
//  Account.swift
//  Shop
//
//  Created by CALC Company on 27/02/2023.
//

import Foundation

struct AccountMe: Codable  {
    let code : Int
    let error: Bool
    let message: String
    let data: Account
}

struct Account: Codable,Identifiable  {
  
    let id : Int
    let name: String
    let username: String
    let avatar: String
    let email: String
    let role: Int
    

}

struct DataRegister {
    var name: String = ""
    var username: String = ""
    var email: String = ""
    var password: String = ""
    var confirmPassword: String = ""
}

struct Forgot {
    var email: String = ""
}

struct OtpData: Codable {
    let otp: String
    let email: String
    let id: Int 
}
