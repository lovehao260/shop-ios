//
//  Token.swift
//  Shop
//
//  Created by CALC Company on 27/02/2023.
//

import Foundation
struct Token: Codable {
    let accessToken: String
    let tokenType: String
    let expiresAt: String
    
    enum CodingKeys: String, CodingKey {
        case accessToken = "access_token"
        case tokenType = "token_type"
        case expiresAt = "expires_at"
    }
}

func login() {
//    @State var email: String = ""
//    @State var password: String = ""
//    let url = URL(string: "http://your-api-endpoint/login")!
//    var request = URLRequest(url: url)
//    request.httpMethod = "POST"
//    request.setValue("application/json", forHTTPHeaderField: "Content-Type")
//    
//    let parameters = ["email": email, "password": password]
//    request.httpBody = try! JSONSerialization.data(withJSONObject: parameters)
//    URLSession.shared.dataTask(with: request) { data, response, error in
//        guard let data = data else {
//            print("Error: No data in response")
//            return
//        }
//        
//        guard let response = response as? HTTPURLResponse else {
//            print("Error: Invalid response")
//            return
//        }
//        
//        if response.statusCode == 200 {
//            do {
//                let token = try JSONDecoder().decode(Token.self, from: data)
//                print("Access Token: \(token.accessToken)")
//            } catch {
//                print("Error: \(error.localizedDescription)")
//            }
//        } else {
//            print("Error: \(response.statusCode)")
//        }
//    }.resume()
}
