//
//  Weather.swift
//  Shop
//
//  Created by CALC Company on 07/03/2023.
//

import Foundation

struct WeatherData: Codable {
    let name: String
    let main: Main
    let weather: [Weather]
    let wind: Wind
    let sys: Sys
}

struct Main: Codable {
    let temp: Double
    let humidity: Double
    let temp_min: Double
    let temp_max: Double
}

struct Weather: Codable {
    let description: String
    let icon: String
    let main: String
}

struct Wind: Codable {
    let speed: Double
}

struct Sys: Codable {
    let sunrise: Int
    let sunset: Int
}
