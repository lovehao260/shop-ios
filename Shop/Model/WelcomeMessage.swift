//
//  WelcomeMessage.swift
//  Shop
//
//  Created by CALC Company on 18/04/2023.
//

import Foundation

struct WelcomeMessage: Hashable {
    var title: String = ""
    var description: String = ""
}
