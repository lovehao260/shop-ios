//
//  Song.swift
//  Shop
//
//  Created by CALC Company on 13/03/2023.
//

import Foundation
import SwiftUI

struct Song: Identifiable, Codable {
    let id: Int
    let name: String
    let artist: String
    let duration: Int
    let coverImage: String
}

struct SongFile: Identifiable, Hashable {
    var id = UUID()
    let imageName: String
    let title: String
    let artist: String
    let time: Int
    let fileName: String
    let fileExtension: String
}

extension SongFile {
    static var all: [SongFile] {
        return [
            SongFile(imageName: "song", title: "Takes It As It Comes", artist: "The Doors", time: 3 * 60, fileName: "song1", fileExtension: "mp3"),
            SongFile(imageName: "song2", title: "On The Road Again", artist: "Willie Nelson", time: 3 * 60,fileName: "song2", fileExtension: "mp3"),
            SongFile(imageName: "song1", title: "Thunderstruck", artist: "ACDC", time: 3 * 60,fileName: "song3", fileExtension: "mp3")
        ]
    }
    
    static var sample: SongFile {
        SongFile(imageName: "theDoors", title: "Takes It As It Comes", artist: "The Doors", time: 3 * 60,fileName: "song1", fileExtension: "mp3")
    }
}
