//
//  PlayList.swift
//  Shop
//
//  Created by CALC Company on 13/03/2023.
//

import Foundation

struct Playlist {
    let name: String
    var songs: [SongFile] = []
}
