//
//  Tab.swift
//  Shop
//
//  Created by CALC Company on 17/03/2023.
//

import Foundation
enum Tab: String, CaseIterable{
    
    case home = "house"
    case weather = "sunrise.fill"
    case music = "music.note.list"
    case user = "person"
    case calculate = "calendar.badge.minus"
}
