//
//  Validate.swift
//  Shop
//
//  Created by CALC Company on 27/02/2023.
//

import Foundation

func isValidEmail(_ email: String) -> Bool {
    let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
    guard let regex = try? NSRegularExpression(pattern: emailRegex) else {
        return false
    }
    let range = NSRange(location: 0, length: email.utf16.count)
    return regex.firstMatch(in: email, options: [], range: range) != nil
}
func isValidNull(_ string: String?) -> Bool {
    guard let string = string else {
        return false
    }
    let trimmed = string.trimmingCharacters(in: .whitespaces)
    return !trimmed.isEmpty
}
func passwordsMatch(password: String, confirmPassword: String) -> Bool {
    return password == confirmPassword
}
func isValidInputLength(_ input: String, min: Int, max: Int) -> Bool {
    return input.count >= min && input.count <= max
}

