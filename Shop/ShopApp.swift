//
//  ShopApp.swift
//  Shop
//
//  Created by CALC Company on 16/02/2023.
//

import SwiftUI

@main
struct ShopApp: App {
    @UIApplicationDelegateAdaptor(AppDelegate.self) var appDelegate
    //    let persistenceController = PersistenceController.shared
    @StateObject private var viewModel = AppViewModel()
    @State private var isFirstLaunch = true
    var body: some Scene {
        WindowGroup {
            if viewModel.shouldShowWelcomeScreen {
                WelcomeUIView(isFirstLaunch: $isFirstLaunch)
                    .onAppear {
                        viewModel.setAppLaunched()
                    }
            } else {
                LoginUIView()
            }
            
            
            //            ContentView()
            //                .environment(\.managedObjectContext, persistenceController.container.viewContext)
        }
    }
}

