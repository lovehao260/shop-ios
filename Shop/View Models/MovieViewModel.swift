//
//  MovieViewModel.swift
//  Shop
//
//  Created by CALC Company on 17/03/2023.
//

import Foundation
import SwiftUI


class MovieViewModel : ObservableObject{
    
    @Published var movieScreen :[TrendingItem] = []
    

}

struct TrendingItem:  Decodable{
    let adult: Bool
    let int: Int
    let poster_path : String
    let title: String
    let vote_average: Float
}
