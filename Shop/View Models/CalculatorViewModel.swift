//
//  CalculatorViewModel.swift
//  Shop
//
//  Created by CALC Company on 14/03/2023.
//

import Foundation
import SwiftUI


enum CalcButton: String {
    case one = "1"
    case two = "2"
    case three = "3"
    case four = "4"
    case five = "5"
    case six = "6"
    case seven = "7"
    case eight = "8"
    case nine = "9"
    case zero = "0"
    case add = "+"
    case subtract = "-"
    case divide = "÷"
    case mutliply = "x"
    case equal = "="
    case clear = "AC"
    case decimal = "."
    case percent = "%"
    case negative = "-/+"
    
    var buttonColor: Color {
        switch self {
        case .add, .subtract, .mutliply, .divide, .equal:
            return .orange
        case .clear, .negative, .percent:
            return Color(.lightGray)
        default:
            return Color(UIColor(red: 55/255.0, green: 55/255.0, blue: 55/255.0, alpha: 1))
        }
    }
}

enum Operation {
    case add, subtract, multiply, divide, none
}


class CalculateViewModel : ObservableObject{
    
    @Published var value = "0"
    
    @Published var numberAfter = "0"
    @Published var numberBefore = "0"
    
    @Published var numberEqual = 0
    @Published var currentOperation: Operation = .none
    
    let buttons: [[CalcButton]] = [
        [.clear, .negative, .percent, .divide],
        [.seven, .eight, .nine, .mutliply],
        [.four, .five, .six, .subtract],
        [.one, .two, .three, .add],
        [.zero, .decimal, .equal],
    ]
    func didTap(button: CalcButton) {
        let number = Int(self.value ) ?? 0
        switch button {
        case .add, .subtract, .mutliply, .divide, .equal:
            if button == .add { // cong
                self.currentOperation = .add
                self.numberAfter = value
                self.numberBefore = "0"
                
            }
            else if button == .subtract { // tru
                self.currentOperation = .subtract
                self.numberAfter = value
                self.numberBefore = "0"
            }
            else if button == .mutliply { // nhan
                self.currentOperation = .multiply
                self.numberAfter = value
                self.numberBefore = "0"
            }
            else if button == .divide { // chia
                self.currentOperation = .divide
                self.numberAfter = value
                self.numberBefore = "0"
            }
            else if button == .equal { // Ket qua
                let runningValue = Double(self.numberAfter) ?? 0
                let currentValue = Double(self.numberBefore) ?? 0
                switch self.currentOperation {
                case .add:
                  
                    self.value = "\( runningValue + currentValue)"
                case .subtract: self.value = "\(runningValue - currentValue)"
                case .multiply: self.value = "\(runningValue * currentValue)"
                case .divide: self.value = "\( String(format: "%.6f", (Double(runningValue) / Double(currentValue))))"
                    
                    
                case .none:
                    break
                }
            }
         
        
        case .percent:
            self.value = "\( Double(number) / 100)"
            self.numberAfter = value
            self.numberBefore = "0"
            //        case .decimal:
            //           break
        case .clear:
            self.value = "0"
            self.numberBefore = "0"
            self.numberAfter = "0"
            self.currentOperation = .none
        default:
            let number = button.rawValue
            if self.currentOperation == .none {
                if self.numberAfter == "0" &&  number != "." {
                    if self.numberAfter == "0." {
                        self.numberAfter = "\(self.value)\(number)"
                    }else{
                        numberAfter = number
                    }
                   
                }
                else {
                    self.numberAfter = "\(self.value)\(number)"
                }
                value = numberAfter
            }
            else {
                if self.numberBefore == "0" &&  number != "." {
                    numberBefore = number
                }
                else {
                    self.numberBefore = "\(self.value)\(number)"
                }
                value = numberBefore
                
            }
        }
    }
    
    func formatNumber(_ number: Double, decimalPlaces: Int, showSeparator: Bool) -> String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.maximumFractionDigits = decimalPlaces
        
        if showSeparator {
            formatter.groupingSeparator = Locale.current.groupingSeparator ?? ","
            formatter.groupingSize = 3
        }
        
        return formatter.string(from: NSNumber(value: number)) ?? ""
    }
    func buttonWidth(item: CalcButton)->CGFloat{
        if item == .zero {
            return ((UIScreen.main.bounds.width - (5*12)) / 4) * 2
        }
        return (UIScreen.main.bounds.width - (5*12)) / 4
    }
    
    func buttonHeight(item: CalcButton)->CGFloat{
        return (UIScreen.main.bounds.width - (5*12)) / 4
    }
}
