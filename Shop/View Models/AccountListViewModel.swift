//
//  AccountListViewModel.swift
//  Shop
//
//  Created by CALC Company on 27/02/2023.
//

import Foundation
import UIKit
class AccountListViewModel: ObservableObject {
    var errors :Bool = false
    @Published var items: [Account] = []
    @Published var user: Account
    @Published var isLogout = false
    init() {
           user = Account( id: 0, name: "", username: "", avatar: "", email: "", role: 0)
       }
    let fieldName = "upload_image"
    
    func getAllAccounts() {
        
        let defaults = UserDefaults.standard
        guard let token = defaults.string(forKey: "Bearer") else {
            return
        }
     
        Webservice().getAllAccounts(token: token) { (result) in
            switch result {
            case .success(let data):
                print(data)
                DispatchQueue.main.async {
                    self.items = data
                }
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
        
    }
    private func createHttpBody(paramaters: [String:Any],binaryData: Data, mimeType: String) -> Data {
        let boundary = UUID().uuidString
        
        var postContent = "--\(boundary)\r\n"
        let fileName = "\(UUID().uuidString).jpeg"
        postContent += "Content-Disposition: form-data; name=\"\(fieldName)\"; filename=\"\(fileName)\"\r\n"
        postContent += "Content-Type: \(mimeType)\r\n\r\n"

        var data = Data()
        guard let postData = postContent.data(using: .utf8) else { return data }
        data.append(postData)
        data.append(binaryData)

        // その他パラメータがあれば追加
        var content = ""
        paramaters.forEach {
            content += "\r\n--\(boundary)\r\n"
            content += "Content-Disposition: form-data; name=\"\($0.key)\"\r\n\r\n"
            content += "\($0.value)"
        }
        if let postData = content.data(using: .utf8) { data.append(postData) }
        
        // HTTPBodyの終了を設定
        guard let endData = "\r\n--\(boundary)--\r\n".data(using: .utf8) else { return data }
        data.append(endData)
        return data
    }
    
    func handlerUpdateUser(id: Int, formData:FormData, selectedImage: UIImage?){
        let defaults = UserDefaults.standard
        guard let token = defaults.string(forKey: "Bearer") else {
            return
        }
       
        Webservice().updateUser(token: token, id: id, formData: formData ,image:selectedImage)
        { (result) in
            switch result {
                case .success(let data):
                    print(data)
                    self.errors = false
              
                    
                case .failure(let error):
                    self.errors = true
                    print(error)
                 
            }
        }
    }
    
    func handlerDeleteUser(id:Int){
        let defaults = UserDefaults.standard
        guard let token = defaults.string(forKey: "Bearer") else {
            return
        }
        Webservice().deleteUser(token: token, id: id )
        { (result) in
            switch result {
                case .success(let data):
                    print(data)
                    self.errors = false
              
                    
                case .failure(let error):
                    self.errors = true
                    print(error)
                 
            }
        }
    }
    
    func signout()  {
        
        let defaults = UserDefaults.standard
 
        guard let token = defaults.string(forKey: "Bearer") else {
            return 
        }
        Webservice().signout(token: token )
        { (result) in
            switch result {
                case .success(let message):
                    DispatchQueue.main.async {
                        defaults.removeObject(forKey: "Bearer")
                        self.isLogout = true
                    
                    }
                case .failure(let error):
                    print("Error: \(error.localizedDescription)")
                  
            }
        }

        
    }
    func getMe(){
        let defaults = UserDefaults.standard
        guard let token = defaults.string(forKey: "Bearer") else {
            return
        }
        Webservice().getUserInfo(token: token )
        { (result) in
            switch result {
                case .success(let user):
                    DispatchQueue.main.async {
                        self.user = user
                    }
                case .failure(let error):
                    print("Error: \(error.localizedDescription)")
           
            }
        }
    }
    
}

