//
//  LocationViewModel.swift
//  Shop
//
//  Created by CALC Company on 07/03/2023.
//

import Foundation
import SwiftUI
import MapKit
import CoreLocationUI

class LocationViewModel : NSObject, ObservableObject, CLLocationManagerDelegate{
    @Published var region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: 40, longitude: 120), span: MKCoordinateSpan(latitudeDelta: 100, longitudeDelta: 100))
    
    @Published var coordinate = CLLocationCoordinate2D(latitude: 37.33182, longitude: -122.03118)
    
    let locationManger = CLLocationManager()
    override init(){
        super.init()
        locationManger.delegate = self
    }
    func requestAllowOnceLocationPermission(){
        locationManger.requestLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let latestLocation = locations.first else{
            return
        }
        DispatchQueue.main.async {
            self.region = MKCoordinateRegion(center: latestLocation.coordinate, span: MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05))
            self.coordinate = latestLocation.coordinate
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error.localizedDescription)
    }
}


