//
//  LoginViewModel.swift
//  Shop
//
//  Created by CALC Company on 27/02/2023.
//

import SwiftUI
import Combine
struct Credentials : Codable{
    var name: String = ""
    var username: String = ""
    var email: String = ""
    var password: String = ""
    var confirmPassword: String = ""
}

class LoginViewModel: ObservableObject {
    @Published var credentials = Credentials(username: "", password: "")
    
    var errorMessage = ""

    var isLoginButtonDisabled: Bool {
        credentials.username.isEmpty || credentials.password.isEmpty
    }
    
    @Published var isAuthenticated: Bool = false
    @Published var showErrorAlert: Bool = false
    @Published var showingLonginScreen: Bool = false

    func login() {
        
        let defaults = UserDefaults.standard
        
        Webservice().login(username: credentials.username, password: credentials.password) { result in
            switch result {
            case .success(let token):
                defaults.setValue(token, forKey: "Bearer")
                DispatchQueue.main.async {
                    self.isAuthenticated = true
                }
            case .failure(let error ):
                self.showErrorAlert = true
                
                print(error.localizedDescription)
                
            }
        }
    }
    
    func signout() {
        
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: "jsonwebtoken")
        DispatchQueue.main.async {
            self.isAuthenticated = false
        }
        
    }
    
    func registerHandler(formData:DataRegister){
        
    
        Webservice().register(formData: formData) { result in
            switch result {
            case .success(let token):
                
                DispatchQueue.main.async {
                    self.showingLonginScreen.toggle()
                }
            case .failure(let error ):
                self.showErrorAlert.toggle()
                
                print(error.localizedDescription)
                
            }
            
   
        }
        
    }
    

    
}


