//
//  TaskViewModel.swift
//  Shop
//
//  Created by CALC Company on 05/06/2023.
//

import Foundation

class TaskViewModel: ObservableObject{
    @Published var storedTasks: [Task] = [
        Task(taskTitle: "Metting", taskDescription: "Discuss team task for the day", taskDate:  .init(timeIntervalSince1970: 1686021359)),
        
        Task(taskTitle: "Study Engligh", taskDescription: "Yamanashi", taskDate:  .init(timeIntervalSince1970: 1686038400)),
        Task(taskTitle: "Study Engligh", taskDescription: "Yamanashi", taskDate:  .init(timeIntervalSince1970: 1686038400)),
        Task(taskTitle: "Study Engligh", taskDescription: "Yamanashi", taskDate:  .init(timeIntervalSince1970: 1686038400)),
        Task(taskTitle: "Study Engligh", taskDescription: "Yamanashi", taskDate:  .init(timeIntervalSince1970: 1686038400)),
        Task(taskTitle: "Study Engligh", taskDescription: "Yamanashi", taskDate:  .init(timeIntervalSince1970: 1686038400))
    ]
    // MARK: Initializing
    @Published var currentWeek:[Date] = []
    @Published var currentDate: Date = Date()
    
    // MARK: Filtering today tasks
    @Published var filteredTask:[Task]?
    
    // MARK: Initializing
    init() {
        fetchCurrentWeek()
        filterTodayTasks()
    }
    //MARK:Filter today task
    func filterTodayTasks(){
        DispatchQueue.global(qos: .userInteractive).async {
            let canlendar = Calendar.current
            
            let filtered = self.storedTasks.filter {
                return canlendar.isDate($0.taskDate, inSameDayAs: self.currentDate)
            }
            DispatchQueue.main.async {
                self.filteredTask = filtered
            }
        }
    }
    
    func fetchCurrentWeek(){
        let today = Date()
        let calendar = Calendar.current
        let week = calendar.dateInterval(of: .weekOfMonth, for: today)
        
        guard let firstWeekDay = week?.start else{
            return
        }
        (1...7).forEach{ day in
            if let weekDay = calendar.date(byAdding: .day, value: day, to: firstWeekDay) {
                currentWeek.append(weekDay)
            }
        }
    }
    
    //MARK: Extraction date
    func extractDate(date: Date, format: String) -> String{
        let formatter = DateFormatter()
        
        formatter.dateFormat = format
        
        return formatter.string(from: date)
    }
    
    // MARK Cheking if current Date is Today
    func isTodayOfWeek(date: Date)->Bool{
        let calendar = Calendar.current
        return calendar.isDate(currentDate, inSameDayAs: date)
    }
}
