//
//  AppViewModel.swift
//  Shop
//
//  Created by CALC Company on 01/06/2023.
//

import Foundation

class AppViewModel: ObservableObject {
    @Published var shouldShowWelcomeScreen: Bool
    
    init() {
        shouldShowWelcomeScreen = !UserDefaults.standard.bool(forKey: "AppLaunched")
    }
    
    func setAppLaunched() {
        UserDefaults.standard.set(true, forKey: "AppLaunched")
        shouldShowWelcomeScreen = false
    }
}
