//
//  SongViewModel.swift
//  Shop
//
//  Created by CALC Company on 13/03/2023.
//

import Foundation
import AVKit


class SongViewModel: ObservableObject{
    @Published var  audioDuration: TimeInterval = 0.0
    
    @Published var  currentTime: TimeInterval = 0.0
    @Published var  drapTime: TimeInterval = 0.0
    
    @Published var songs: [SongFile] = SongFile.all
    @Published  var isUpdatingCurrentTime: Bool = false
    
    @Published var currenSong: SongFile = SongFile.all[0]{
        didSet {
            setupSong(currenSong)
        }
    }
    
    @Published var audioPlayer : AVAudioPlayer!
    @Published var timer: Timer?
    
   
    
    init(){
        setupSong(songs[0])
    }
    
    func setupSong (_ song: SongFile){
        let songUrl = Bundle.main.url(forResource: song.fileName, withExtension: song.fileExtension)!
        self.audioDuration = audioPlayer?.duration ?? 0.0
        self.audioPlayer = try! AVAudioPlayer(contentsOf: songUrl)
    }
    
    var currentIndex: Int{
        return songs.firstIndex(of: currenSong) ?? 0
    }
    var lastIndex: Int {
        return songs.count - 1
    }
    
    func previousSong(){
        let previousIndex  = currentIndex == 0 ? lastIndex : currentIndex - 1
        self.currenSong = songs[previousIndex]
        playSong()
    }
    
    func nextSong(){
        let nextIndex  = currentIndex == lastIndex ? 0 : currentIndex + 1
        self.currenSong = songs[nextIndex]
        playSong()
    }
    
    func playSong(){
        audioPlayer?.prepareToPlay()
        audioDuration = audioPlayer?.duration ?? 0.0
        currentTime = audioPlayer.currentTime
        audioPlayer?.play()
        startUpdatingCurrentTime()
        objectWillChange.send()
        
    }
    func pauseSong(){
        audioPlayer?.pause()
        objectWillChange.send()
    }
    
    func stopSong (){
        audioPlayer.stop()
    }
    func formatTime(_ time: TimeInterval) -> String {
        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.minute, .second]
        formatter.zeroFormattingBehavior = .pad
        return formatter.string(from: time) ?? "0:00"
    }
    func updateCurrentTimer() {
        if  isUpdatingCurrentTime {
            currentTime = audioPlayer?.currentTime ?? 0
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.updateCurrentTimer()
            }
        }
    }
    func startUpdatingCurrentTime() {
        isUpdatingCurrentTime = true
        updateCurrentTimer()
    }
    
    func stopUpdatingCurrentTime() {
        isUpdatingCurrentTime = false
        timer?.invalidate()
        timer = nil
    }
    
    ///////////////////Api Song '
    @Published var musicTracks: [MusicTrack] = []

    func fetchSongList(){
        
        SongService().fetchTrendingMusic { tracks, error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            } else if let tracks = tracks {
//                self.musicTracks = tracks
            }
        }
    }
}
