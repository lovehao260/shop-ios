//
//  WeatherViewModel.swift
//  Shop
//
//  Created by CALC Company on 07/03/2023.
//

import SwiftUI
import Foundation
import SwiftUI
import MapKit
import CoreLocation

private let iconMap = [
    "Drizzle" : "cloud.drizzle.fill",
    "Thunderstorm" : "cloud.bolt.rain.fill",
    "Rain": "cloud.rain.fill",
    "Snow": "snow",
    "Clear": "sun.max.fill",
    "Clouds" : "cloud.fill",
]

class WeatherViewModel : NSObject, ObservableObject, CLLocationManagerDelegate{
    
    @Published var city: String = ""
    @Published var temperature: String = "__"
    @Published var tempMin: String = "__"
    @Published var tempMax: String = "__"
    @Published var condition: String = ""
    @Published var windSpeed: Double = 0.0
    @Published var humidity: Double = 0.0
    @Published var sunrise: Date = Date()
    @Published var sunset: Date = Date()
    @Published var weatherIcon: String = "cloud.rain.fill"
    @State var userLocation: CLLocationCoordinate2D?
    
    @Published var region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: 26.212313, longitude: 127.679153), span: MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1))
    var locationManager : CLLocationManager?
    
    // Check location in iphone and set permission location
    func checkIfLocationServiceIsEnabled(){
        if CLLocationManager.locationServicesEnabled(){
            self.locationManager = CLLocationManager()
            self.locationManager!.delegate = self
            
        }else{
            print("Show an alert letting them know this is off and to go turn it on.")
        }
    }
    
    private func checkLocationAuthorization(){
        guard let locationManager = locationManager else { return }
        switch locationManager.authorizationStatus{
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
        case .restricted:
            print("Your location is  restricted likey due to parental controls.")
        case .denied:
            print("You have denied this app location permmission. Go in to settings to change it.")
        case . authorizedAlways, .authorizedWhenInUse:
            region = MKCoordinateRegion(center: locationManager.location!.coordinate, span: MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05))
            
        @unknown default:
            break
            
        }
    }
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        checkLocationAuthorization()
    }
    
    // Call Api weather
    let weatherService = WeatherService()
    
    func callWeather(coordinates: CLLocationCoordinate2D){
        
        weatherService.getWeather(forCoordinates: coordinates) { result in
            switch result {
            case .success(let weatherData):
                DispatchQueue.main.async {
                    self.city = weatherData.name
                    self.temperature = String(format: "%.0f°C", weatherData.main.temp)
                    self.tempMin = String(format: "%.0f", weatherData.main.temp_min)
                    self.tempMax = String(format: "%.0f", weatherData.main.temp_max)
                    self.condition = weatherData.weather.first?.description ?? ""
                    self.windSpeed = weatherData.wind.speed
                    self.humidity = weatherData.main.humidity
                    self.sunrise = Date(timeIntervalSince1970: TimeInterval(weatherData.sys.sunrise))
                    self.sunset = Date(timeIntervalSince1970: TimeInterval(weatherData.sys.sunset))
                    let icon = weatherData.weather.first?.main ?? ""
                    self.weatherIcon = iconMap[icon] ?? ""
                }
            case .failure(let error):
                print("Error: \(error.localizedDescription)")
            }
        }
    }
    
}
