//
//  ForgotPasswordViewModel.swift
//  Shop
//
//  Created by CALC Company on 22/03/2023.
//

import SwiftUI
import Combine

class ForgotPasswordViewModel: ObservableObject {
    @Published var emailSend:String = ""
    @Published var userID:Int = 0
    @Published var otpCode: String = ""
    @Published var DataForgot = OtpData(otp: "", email: "", id: 0)
    
    @Published var code = "";
    @Published  var showingSheetOtp = false
    @Published var showErrorAlert: Bool = false
    var errorMessage = ""
    @Published var progressSend: Bool = false;
    @Published var moveToLogin: Bool = false;
    @Published var isLoading = false
    func forgotPasswordEmail(emailForgot:Forgot){
        self.progressSend = true;
        Webservice().forgotPasswordEmail(emailForgot: emailForgot){  result in
            switch result {
            case .success(let otp):
                DispatchQueue.main.async {
                    if (otp.error){
                        self.showErrorAlert = true
                        self.errorMessage = otp.message
                        self.progressSend = false;
                    }else{
                        
                        self.DataForgot = otp.data
                        self.showingSheetOtp = true
                        self.progressSend = false;
                        
                    }
                    
                }
            case .failure(let error):
                self.showErrorAlert = true
                self.errorMessage = "Errors system"
                print(error.localizedDescription)
                
            }
            self.isLoading.toggle()
        }
        
    }
    
    func changePassword(formFields:PasswordChang){
        
        Webservice().changePassword(formFields: formFields) { result in
            
            switch result {
                case .success(let success):
                if (success.error){
                    self.showErrorAlert = true
                    self.errorMessage = success.message
                }else{
                    
                    self.moveToLogin.toggle()
                    self.progressSend.toggle()
                    
                }
                case .failure(let error ):
                    print(error.localizedDescription)
            }
        }
        
    }
}
